#include "ImageStorage.h"

std::map<std::string, sf::Texture*> ImageStorage::mapToTextures;

void ImageStorage::loadTexture(char* name, std::string reference)
{
  sf::Texture* texture = new sf::Texture();
  texture->loadFromFile(name);
  ImageStorage::mapToTextures[reference] = texture;
}

sf::Texture* ImageStorage::getTexture(std::string reference)
{
  return ImageStorage::mapToTextures[reference];
}