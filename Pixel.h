#ifndef PIXEL_H
#define PIXEL_H

#include "Object.h"
#include "PlayWindow.h"

class Pixel : public Object
{
  public:
    Pixel(int X, int Y, int velX, int velY, sf::Texture* texture, World& w, PlayWindow& window);
    void collidePixels(float elapsedTime);
    bool collide(float elapsedTime, bool react);
    void collideCharacters(float elapsedTime);
    void collideGround(float elapsedTime);
    int update(float elapsedTime, int index);
    void updatePositionAfterGroundCollision(sf::Rect<float> objPos, float elapsedTime);
    void addVelocity(sf::Vector2f velocityToAdd);

  protected:
  	float creationTime;

};

#endif
