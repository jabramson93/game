#include "EndMenu.h"

#include <iostream>
#include <sstream>

#include "Setting.h"
#include "StartMenu.h"
#include "PlayWindow.h"
#include "Game.h"
#include "WinScreen.h"

#include<stdio.h>


EndMenu::EndMenu(PlayWindow* windowTemp, Game* previousTemp, int side, int round, int player1NumberWinsTemp, int player2NumberWinsTemp) : Playable()
{
  previous = previousTemp;
  player1NumberWins = player1NumberWinsTemp;
  player2NumberWins = player2NumberWinsTemp;
  if( side == 1 )
    player1NumberWins++;
  else if ( side == 2 )
    player2NumberWins++;

  nextRound = round + 1;

  window = windowTemp;
  font.loadFromFile("Minecraftia.ttf");
  std::ostringstream convert1;
  tournamentFinished = nextRound > 3;
  if( side != -1 )
  {
    char fileName[50];
    sprintf(fileName, "text/round_%i_player%i_win.png", round, side);
    textureText.loadFromFile(fileName);
  }
  else
  {
    textureText.loadFromFile("text/tie.png");
  }

  text.setTexture(textureText);
  text.setScale(Setting::scaling.x, Setting::scaling.y);
  text.setPosition( window->getRealSize().x / 2 - text.getGlobalBounds().width / 2, window->getRealSize().y / 2 - text.getGlobalBounds().height / 2 );
}

Playable* EndMenu::play()
{
  while( window->isOpen() )
  {
    sf::Event event;
    while (window->pollEvent(event))
    {
        switch (event.type)
        {
            case sf::Event::KeyPressed:
                if( event.key.code == sf::Keyboard::Escape)
                    window->close();
                else if( event.key.code == sf::Keyboard::Return)
                {
                    if( tournamentFinished )
                    {
                        delete previous;
                        return new WinScreen(window, this, player1NumberWins, player2NumberWins);
                    }
                    else
                    {
                        delete previous;
                        return new Game(window, this, nextRound, player1NumberWins, player2NumberWins);
                    }
                }
                break;

            case sf::Event::JoystickButtonPressed:
                if( event.joystickButton.button == 0)
                {
                    if( tournamentFinished )
                    {
                        delete previous;
                        return new WinScreen(window, this, player1NumberWins, player2NumberWins);

                    }
                    else
                    {
                        delete previous;
                        return new Game(window, this, nextRound, player1NumberWins, player2NumberWins);
                    }
                }
                break;
        }
    }
    if( window->isOpen() )
    {
      window->clear();
      previous->draw();
      sf::RectangleShape black(sf::Vector2f(window->getRealSize().x, window->getRealSize().y) );
      black.setFillColor(sf::Color(0,0,0,200));
      window->draw(black);
      window->draw(text);
      window->display();
    }
  }
  return NULL;
}
