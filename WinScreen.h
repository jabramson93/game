#ifndef WINSCREEN_H
#define WINSCREEN_H

#include <SFML/Graphics.hpp>
#include "Playable.h"
#include "PlayWindow.h"

class WinScreen  : public Playable
{
    public:
        WinScreen(PlayWindow* windowTemp, Playable* previous,  int player1NumberWinsTemp, int player2NumberWinsTemp);
        Playable* play();
    protected:
        PlayWindow* window;
        sf::Sprite sprite,spriteText, spriteBackground, spriteChar, spriteArm;
        sf::Texture texture,textureText, textureBackground, textureChar, textureArm;
        sf::Image image;
        int player1NumberWins, player2NumberWins;
        int offset;
        sf::Sprite* sprites;
        sf::Clock clock;
        sf::Vector2f dimension;
        float timeElapsed;
        int numberFrames, index;
    private:
};

#endif // WINSCREEN_H
