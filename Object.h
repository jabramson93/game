#ifndef OBJECT_H
#define OBJECT_H

#include <SFML/Graphics.hpp>
#include <string>

#include "World.h"
#include "PlayWindow.h"

class Object
{
  public:
    Object(int X, int Y, int velX, int velY, sf::Texture* texture, World& w, PlayWindow& window);

    virtual void draw(PlayWindow* window);
    virtual int update(float elapsedTime);

    // Part of the code to handle collisions:
    virtual bool collide(float elapsedTime, bool react) = 0;

    virtual void reactCollisionPixel(Pixel* p);
    virtual void reactCollisionShot(Shot* s);
    virtual void reactCollisionCharacter(Character* c);
    virtual void reactWallCollision();

    virtual void updatePositionAfterCollision(Object* o, float elapsedTime);

    virtual void addVelocity(sf::Vector2f velocityToAdd);
    void setVelocity(sf::Vector2f newVelocity);
    void setVelocityX(float velX);

    bool collision(Object* object);
    virtual void checkObjectIsInBound(float timeElapsed);
    bool isOnGround();
    virtual sf::FloatRect getBoundingBox();

    int collisionState;
    sf::Sprite sprite;
    sf::Vector2f position, dimension;


  protected:
    PlayWindow& window;
    sf::Vector2f lastPosition, velocity;
    int gravity;

    World& world;

    bool bouncing;

};

#endif
