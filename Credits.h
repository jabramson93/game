#ifndef CREDITS_H
#define CREDITS_H

#include <SFML/Graphics.hpp>
#include "Playable.h"
#include "PlayWindow.h"

class Credits : public Playable
{
    public:
        Credits(PlayWindow* windowTemp);
        Playable* play();
        void update(float offset);

    protected:
        PlayWindow* window;
        int startPosition, numberText;
        sf::Font font;
        sf::Text** texts;
        sf::Clock clock;
};

#endif
