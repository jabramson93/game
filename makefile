all: main

main: clean main.o game.o object.o character.o shot.o world.o imageStorage.o pixel.o ground.o setting.o
	g++ ImageStorage.o World.o Object.o Pixel.o Shot.o Character.o Ground.o Game.o main.o Setting.o -o pixel-fighter -L ../SFML-2.1/lib/ -lsfml-graphics -lsfml-window -lsfml-system

setting.o:
	g++ -c Setting.cpp -I ../SFML-2.1/include

ground.o:
	g++ -c Ground.cpp -I ../SFML-2.1/include

pixel.o:
	g++ -c Pixel.cpp -I ../SFML-2.1/include

world.o: 
	g++ -c World.cpp -I ../SFML-2.1/include

imageStorage.o:
	g++ -c ImageStorage.cpp -I ../SFML-2.1/include

game.o:
	g++ -c Game.cpp -I ../SFML-2.1/include

shot.o:
	g++ -c Shot.cpp -I ../SFML-2.1/include

character.o: 
	g++ -c Character.cpp -I ../SFML-2.1/include

object.o:
	g++ -c Object.cpp -I ../SFML-2.1/include

main.o:
	g++ -c main.cpp -I ../SFML-2.1/include

clean:
	rm -rf *o pixel-fighter
