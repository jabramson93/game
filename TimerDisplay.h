#include "PlayWindow.h"
#include <SFML/Graphics.hpp>

class TimerDisplay
{
    public:
        TimerDisplay(PlayWindow* windowTemp);
        void update(float time);
        void draw();
        int getDigit(int number, int decimal);
        void center();
        int getStart(int index);

    protected:
        int timeOld;
        int size[10];
        PlayWindow* window;
        float xSize, ySize;
        sf::Texture texture;
        sf::Sprite* sprites;
        sf::Sprite sprite1, sprite0;
};
