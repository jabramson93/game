#include <iostream>
#include <cmath>
#include <string>
#include <stdlib.h>
#include <time.h>

#include "Shot.h"
#include "Pixel.h"
#include "Character.h"
#include "ImageStorage.h"
#include "Setting.h"
#include "Game.h"
#include "ShotType.h"
#include "PlayWindow.h"

#define PI (3.141592653589793)

Shot::Shot(int X, int Y, int velX, int velY, sf::Texture* texture, World& w, PlayWindow& window, float firingAngle, Object* parentT, float chargeTimeTemp, ShotType typeTemp, int side) : Object( X, Y, velX, velY, texture, w, window)
{
  numberOfPixels = 10 + chargeTimeTemp * CHARGE_COEFFICIENT;
  if( numberOfPixels > MAXIMUM_NUMBER_PIXELS_PER_SHOT)
    numberOfPixels = MAXIMUM_NUMBER_PIXELS_PER_SHOT;

  if( typeTemp == NORMAL )
  {
    textures[0] = ImageStorage::getTexture(std::string("shotBlue"));
    textures[1] = ImageStorage::getTexture(std::string("shotBlue"));
  }
  else if ( typeTemp == RICOCHET )
  {
    textures[0] = ImageStorage::getTexture(std::string("shotGreen"));
    textures[1] = ImageStorage::getTexture(std::string("shotGreen"));
  }
  else if ( typeTemp == DESTRUCTION )
  {
    textures[0] = ImageStorage::getTexture(std::string("shotRed"));
    textures[1] = ImageStorage::getTexture(std::string("shotRed"));
  }

  angle = firingAngle * 180 / 3.14159265;
  sprite.setTexture(*textures[0]);
  sprite.setScale(sf::Vector2f(Setting::scaling.x, Setting::scaling.y));
  sprite.setOrigin(0, 5);
  sprite.setRotation(angle);
  state = 1;
  gravity = 0;
  timeElapsed = 0;
  frame = 0;
  parent = parentT;
  bounces = 1;
  type = typeTemp;
  dimension.x = sprite.getGlobalBounds().width;
  dimension.y = sprite.getGlobalBounds().height;
}

void Shot::collideGround()
{
  if( world.checkGroundCollision(this->getBoundingBox(), 0) )
  {
    if( type == RICOCHET || type == NORMAL)
    {
      if( type != RICOCHET|| bounces > NUMBER_OF_BOUNCES )
      {
        reactCollision();
      }
      else
      {
        bounces++;
        bounce(world.getLastGroundCollision());
        float angle = atan2( velocity.y, velocity.x) * 180 / PI;
        sprite.setRotation(angle);
      }
    }
    else if( type == DESTRUCTION)
    {
      world.destroyPixels(getBoundingBox());
      state = -1;
    }
  }
}

void Shot::bounce(sf::Rect<float> box)
{
  sf::Vector2f oldPosition = lastPosition;
  if( !( oldPosition.x + dimension.x < box.left || oldPosition.x > box.left + box.width ) )
  {
    velocity.y = -velocity.y;
    position.y = lastPosition.y;
  }
  else if( !( oldPosition.y + dimension.y < box.top || oldPosition.y > box.top + box.height ) )
  {
    velocity.x = -velocity.x;
    position.x = lastPosition.x;
  }
  else
  {
    velocity.x = -velocity.x;
    velocity.y = -velocity.y;
    position = lastPosition;
  }
  sprite.setPosition(position);
}

int Shot::update(float elapsedTimeTemp)
{
  timeElapsed += elapsedTimeTemp;
  if( timeElapsed > SHOT_ANIMATION_TIME )
  {
    sprite.setTexture(*textures[frame]);
    if( frame == 1 )
      frame = 0;
    else
      frame = 1;

    timeElapsed = 0;
  }
  Object::update(elapsedTimeTemp);
  return state;
}

void Shot::checkObjectIsInBound( float timeElpased)
{
  if( position.y + dimension.y > window.getSize().y )
  {
    velocity.y = -velocity.y;
    position.y = window.getSize().y - dimension.y;
    reactWallCollision();
  }
  else if ( position.y < 0 )
  {
    velocity.y = -velocity.y;
    position.y = 0;
    reactWallCollision();
  }

  if( position.x + dimension.x > window.getSize().x )
  {
    velocity.x = -velocity.x;
    position.x = window.getSize().x - dimension.x;
    reactWallCollision();
  }
  else if ( position.x < 0 )
  {
    velocity.x = -velocity.x;
    position.x = 0;
    reactWallCollision();
  }
}

void Shot::reactCollision()
{
  if( state == -1 || type == DESTRUCTION )
    return;
  int counter = 0;
  for( float i = 0; i < 360 ; i += ( 360 / ((float)numberOfPixels) ) )
  {
    counter++;
    float velX = cos((float)i) * PIXEL_SPEED;
    float velY = sin((float)i) * PIXEL_SPEED;
    world.add(new Pixel(position.x, position.y, velX, velY, ImageStorage::getTexture(std::string("pixel")), world, window));
  }
  state = -1;
}

sf::FloatRect Shot::getBoundingBox()
{
  sf::FloatRect boundingBox = sprite.getGlobalBounds();
  if( velocity.x > 0 )
    boundingBox.left = boundingBox.left + boundingBox.width - SHOT_BOX_SIZE_X;
  if( velocity.y > 0 )
    boundingBox.top = boundingBox.top + boundingBox.height - SHOT_BOX_SIZE_Y;
  boundingBox.width = SHOT_BOX_SIZE_X;
  boundingBox.height = SHOT_BOX_SIZE_Y;
  return boundingBox;
}

sf::Vector2f Shot::getWallCollisionVector()
{
  if( position.y + dimension.y > window.getSize().y || position.y < 0)
  {
    return sf::Vector2f( velocity.x, -velocity.y );
  }
  if( position.x + dimension.x > window.getSize().x )
  {
    return sf::Vector2f( -velocity.x, velocity.y );
  }
}

void Shot::reactCollision(sf::Vector2f meanVelocity, float dispersion)
{
  if( state == -1 )
    return;
  srand (time(NULL));
  for( int i = 0; i < 360; i += 36)
  {
   /* meanVelocity.x = meanVelocity.x / ( meanVelocity.x + meanVelocity.y);
    meanVelocity.y = meanVelocity.y / ( meanVelocity.x + meanVelocity.y);*/
    float angle = atan2( meanVelocity.y, meanVelocity.x) ;
    float velX = cos(angle) * 50;
    float velY = sin(angle) * 50;
    world.add(new Pixel(position.x, position.y, velX, velY, ImageStorage::getTexture(std::string("pixel")), world, window));
  }
  state = -1;
}


bool Shot::collide(float elapsedTime, bool react)
{
  collideCharacters();
  collideShots();
  collideGround();
}

void Shot::collideShots()
{
  // Test collisions with characters:
  for( int i = 0; i < world.getNumberShots() ; i++ )
    if( this != world.getShot(i) && collision((Object*)world.getShot(i) ))
    {
      reactCollisionShot(world.getShot(i));
      world.getShot(i)->reactCollisionShot(this);
    }
}

void Shot::collideCharacters()
{
  // Test collisions with characters:
  for( int i = 0; i < world.getNumberCharacters() ; i++ )
    if( (Object*)world.getCharacter(i) != parent && collision((Object*)world.getCharacter(i) ))
    {
      reactCollisionCharacter(world.getCharacter(i));
      world.getCharacter(i)->reactCollisionShot(this);
      if( type == NORMAL )
        world.getCharacter(i)->takeDamage(DAMAGE);
      else if( type == RICOCHET )
        world.getCharacter(i)->takeDamage(DAMAGE/2);
      else if ( type == DESTRUCTION )
        world.getCharacter(i)->takeDamage(DAMAGE*2);
    }
}

void Shot::reactWallCollision()
{
  //reactCollision(getWallCollisionVector(), 3);
  if( bounces > NUMBER_OF_BOUNCES || type == NORMAL )
  {
    reactCollision();
  }
  else if( type == RICOCHET)
  {
    float angle = atan2( velocity.y, velocity.x) * 180 / PI;
    sprite.setRotation(angle);
    bounces++;
  }
  else
  {
      state = -1;
  }
}

void Shot::reactCollisionShot(Shot* s)
{
  reactCollision();
}

void Shot::reactCollisionCharacter(Character* c)
{
  //reactCollision();
  state = -1;
}
