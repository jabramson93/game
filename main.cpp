#include <SFML/Graphics.hpp>
#include <iostream>


#include "SplashScreen.h"
#include "StartMenu.h"
#include "Setting.h"
#include "PlayWindow.h"

int main()
{
    sf::RenderWindow window(sf::VideoMode(200, 200), "Pixel Leak", sf::Style::Fullscreen);
    //sf::RenderWindow window(sf::VideoMode(800, 600), "Pixel Fighters");

    Setting::scaling.x = window.getSize().x / ((float)1366);
    Setting::scaling.y = window.getSize().y / ((float)768);
    if( Setting::scaling.x > Setting::scaling.y )
        Setting::scaling.x = Setting::scaling.y;
    else
        Setting::scaling.y  = Setting::scaling.x;

    window.setMouseCursorVisible(false);
    PlayWindow playWindow(&window);

    Playable* playing = new SplashScreen(&playWindow);
    while( playing != NULL )
    {
        playing = playing->play();
    }

    return 0;
}
