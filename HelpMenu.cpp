#include "HelpMenu.h"

#include "StartMenu.h"
#include "Setting.h"
#include "PlayWindow.h"
#include <iostream>


HelpMenu::HelpMenu(PlayWindow* windowTemp) : Playable()
{
    window = windowTemp;
    texture.loadFromFile("Help_Menu.png");
    sprite.setTexture(texture);
    sprite.setScale(Setting::scaling.x, Setting::scaling.y);
    textureButton.loadFromFile("B_button.png");
    spriteButton.setTexture(textureButton);
    // spriteButton.setOrign();
    spriteButton.setScale(Setting::scaling.x, Setting::scaling.y);
    spriteButton.setPosition(window->getRealSize().x / 2 - spriteButton.getGlobalBounds().width / 2, window->getRealSize().y - spriteButton.getGlobalBounds().height);
}

Playable* HelpMenu::play()
{
    while(window->isOpen())
    {
        sf::Event event;
        while (window->pollEvent(event))
        {
            switch (event.type)
            {
                case sf::Event::KeyPressed:
                    if( event.key.code == sf::Keyboard::Escape)
                    {
                        window->close();
                        return NULL;
                    }
                    else if( event.key.code == sf::Keyboard::Space)
                    {
                        StartMenu::getInstance(window)->addToRemove(this);
                        return StartMenu::getInstance(window);
                    }
                    break;

                case sf::Event::JoystickButtonPressed:
                    if ( event.joystickButton.button == 1 )
                    {
                        StartMenu::getInstance(window)->addToRemove(this);
                        return StartMenu::getInstance(window);
                    }
                    break;
            }
        }
        window->clear();
        window->draw(sprite);
        window->draw(spriteButton);
        window->display();
    }
    return NULL;
}

