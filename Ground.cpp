#include "Ground.h"

#include <cmath>
#include <iostream>

#include "ImageStorage.h"
#include "World.h"
#include "Object.h"
#include "Character.h"
#include "Pixel.h"
#include "Setting.h"
#include "PlayWindow.h"

Ground::Ground(int numberOfSquareTemp, int numberOfverticalSquaresTemp, sf::Vector2f sizeOfSquare, PlayWindow* w, World* wo) : groudSprites(numberOfSquareTemp), groudSpritesColor(numberOfSquareTemp), groudSpritesShadow(numberOfSquareTemp)
{
  numberOfverticalSquares = numberOfverticalSquaresTemp;
  world = wo;
  radiusX = sizeOfSquare.x / 2;
  radiusY = 2 * Setting::scaling.y;
  lastI = 0;
  lengthI = 0;
  window = w;
  sizeSquare = sizeOfSquare;
  numberOfSquare = numberOfSquareTemp;
  heights = (int*)malloc(sizeof(int)* numberOfSquare);
  for( int i = 0; i < numberOfSquare; i++ )
  {
    heights[i] = 0;
  }
}

void Ground::emergencyAdd(sf::Rect<float> pos, int indexPixel)
{
  int startX = ( pos.left - radiusX ) / sizeSquare.x;
  if( startX < 0 )
    startX = 0;
  int endX = ( pos.left + pos.width + radiusX) / ((float)sizeSquare.x);
  if( endX >= numberOfSquare)
    endX = numberOfSquare - 1;
  int bestI = -1;
  float distanceI;
  for( int i = startX; i <= endX ; i++)
  {
    if( pos.top + pos.height < window->getSize().y - heights[i] * sizeSquare.y
        && ( bestI == -1 || distanceI > abs( ( i - 1 ) * sizeSquare.x + sizeSquare.x / 2 - pos.left - pos.width / 2 ) ) )
    {
        bestI = i;
        distanceI =  abs( ( i - 1 ) * sizeSquare.x + sizeSquare.x / 2 - pos.left - pos.width / 2 );
    }
  }

  if( bestI != -1 )
    addPixel(bestI, indexPixel);
}

void Ground::addPixel(int i, int indexPixel)
{
  sf::Rect<float>  nextPosition(i * sizeSquare.x, window->getSize().y - ( heights[i] + 1 ) * sizeSquare.y, sizeSquare.x, sizeSquare.y);

  emptySpace(nextPosition, indexPixel);

  heights[i]++;

  sf::Sprite newPixel;
  newPixel.setTexture(*ImageStorage::getTexture(std::string("pixel")));
  newPixel.setOrigin(newPixel.getGlobalBounds().width / 2, newPixel.getGlobalBounds().height / 2);
  newPixel.setScale(sf::Vector2f((PIXEL_SIZE / ORIGINAL_PIXEL_SIZE) * Setting::scaling.x, (PIXEL_SIZE / ORIGINAL_PIXEL_SIZE) * Setting::scaling.y));
  newPixel.setPosition( newPixel.getGlobalBounds().width / 2 + i * sizeSquare.x, newPixel.getGlobalBounds().height / 2 - heights[i] * sizeSquare.y + window->getSize().y);
  newPixel.rotate(rand() / (  RAND_MAX / 4 ) * 90 );
  groudSprites.at(i).push_back(newPixel);

  sf::Sprite newPixelShadow;
  newPixelShadow.setTexture(*ImageStorage::getTexture(std::string("pixel_shadow")));
  newPixelShadow.setOrigin(newPixelShadow.getGlobalBounds().width / 2, newPixelShadow.getGlobalBounds().height / 2);
  newPixelShadow.setScale(sf::Vector2f((PIXEL_SIZE / ORIGINAL_PIXEL_SIZE) * Setting::scaling.x, (PIXEL_SIZE / ORIGINAL_PIXEL_SIZE) * Setting::scaling.y));
  newPixelShadow.setPosition( newPixelShadow.getGlobalBounds().width / 2 + i * sizeSquare.x, newPixelShadow.getGlobalBounds().height / 2 - heights[i] * sizeSquare.y + window->getSize().y);
  groudSpritesShadow.at(i).push_back(newPixelShadow);

  sf::Sprite newPixelColor;
  newPixelColor.setTexture(*ImageStorage::getTexture(std::string("pixel")));
  newPixelColor.setColor(sf::Color ( 255, 0, 0, ( heights[i] / ((float)numberOfverticalSquares) ) * 255));
  newPixelColor.setScale(sf::Vector2f( (PIXEL_SIZE / ORIGINAL_PIXEL_SIZE) * Setting::scaling.x, (PIXEL_SIZE / ORIGINAL_PIXEL_SIZE) * Setting::scaling.y));
  newPixelColor.setPosition( i * sizeSquare.x, - heights[i] * sizeSquare.y + window->getSize().y);
  groudSpritesColor.at(i).push_back(newPixelColor);
}

bool Ground::add(sf::Rect<float> pos, int indexPixel)
{
  int startX = ( pos.left - radiusX ) / sizeSquare.x;
  if( startX < 0 )
    startX = 0;

  int endX = ( pos.left + pos.width + radiusX) / ((float)sizeSquare.x);
  if( endX >= numberOfSquare)
    endX = numberOfSquare - 1;

  for( int i = startX; i <= endX ; i++)
  {
  	if( abs( window->getSize().y - heights[i] * sizeSquare.y - pos.top  - pos.height ) <= radiusY )
    {
      if( abs( ( i - 1 ) * sizeSquare.x + sizeSquare.x / 2 - pos.left - pos.width / 2 ) <= radiusX )
      {
        if(    i - 1 > 0
            && ( i - 1 ) * sizeSquare.x + sizeSquare.x / 2 - pos.left - pos.width / 2 < -2 * Setting::scaling.x
            && heights[i] - heights[i-1] > 3)
          world->getPixel(indexPixel)->setVelocityX(100);
        else if (     i + 1 < numberOfSquare
                  && ( i - 1 ) * sizeSquare.x + sizeSquare.x / 2 - pos.left - pos.width / 2 > 2 * Setting::scaling.x
                  && heights[i] - heights[i+1] > 3)
          world->getPixel(indexPixel)->setVelocityX(-100);
        else
        {
          addPixel(i, indexPixel);
          return true;
        }
      }
      else
      {
        if( ( i - 1 ) * sizeSquare.x + sizeSquare.x / 2 - pos.left - pos.width / 2 <  0)
          world->getPixel(indexPixel)->setVelocityX(50);
        else
          world->getPixel(indexPixel)->setVelocityX(-50);
      }
    }
  }
  return false;
}

bool Ground::removeLayer()
{
  for( int i = 0; i++; i < numberOfSquare )
    heights[i]--;
}

void Ground::draw()
{
  for( int i = 0; i < groudSprites.size(); i++ )
    for( int j = 0; j < groudSprites.at(i).size(); j++)
    {
      window->draw(groudSprites.at(i).at(j), true);
      window->draw(groudSpritesShadow.at(i).at(j), true);
      window->draw(groudSpritesColor.at(i).at(j), true);
    }
}

void Ground::emptySpace(sf::Rect<float> pos, int indexPixel)
{
  for( int i = 0; i < world->getNumberPixels() ; i++ )
    if( i != indexPixel && world->getPixel(i)->getBoundingBox().intersects(pos) )
      world->getPixel(i)->position.y = pos.top - world->getPixel(i)->getBoundingBox().height - 1;

  for( int i = 0; i < world->getNumberCharacters() ; i++ )
    if( world->getCharacter(i)->getBoundingBox().intersects(pos) )
      world->getCharacter(i)->position.y = pos.top - world->getCharacter(i)->getBoundingBox().height - 1;
}


bool Ground::collision(sf::Rect<float> pos, int error)
{
  int startX = ( pos.left + error ) / sizeSquare.x;
  if( startX < 0 )
    startX= 0;
  int endX = ( pos.left + pos.width - error ) / ((float)sizeSquare.x) ;
  if( endX >= numberOfSquare)
    endX = numberOfSquare - 1;

  for( int i = startX; i <= endX; i++)
  {
    float currentHeight = window->getSize().y - heights[i] * sizeSquare.y;
    float objectHeight = pos.top + pos.height;
    if( currentHeight <= objectHeight )
    {
      lastI = i;
      int j = i;
      lengthI = 1;
      while( j+1 < numberOfSquare && heights[j] == heights[j+1])
      {
        j++;
        lengthI++;
      }
      if( i == startX )
      {
        j = i;
        while( j-1 > 0 && heights[j] == heights[j-1])
        {
          j--;
          lengthI++;
          lastI--;
        }
      }
      return true;
    }
  }
  return false;
}

void Ground::destroy(sf::Rect<float> pos)
{
  int startX = ( pos.left - EXPLOSION_RADIUS ) / sizeSquare.x;
  if( startX < 0 )
    startX = 0;
  int endX = ( pos.left + pos.width + EXPLOSION_RADIUS ) / ((float)sizeSquare.x) ;
  if( endX >= numberOfSquare)
    endX = numberOfSquare - 1;

  for( int i = startX; i <= endX; i++)
  {
    int start = -1;
    int end = -1;
    for( int j = 0; j < heights[i]; j++)
    {
        double distancey = ( pos.top + pos.height / 2 ) - ( window->getSize().y - j * sizeSquare.y - sizeSquare.y / 2 );
        double distancex = ( pos.left + pos.width / 2 ) - ( i - 1) * sizeSquare.x - sizeSquare.x / 2;
        if( distancex * distancex + distancey * distancey < EXPLOSION_RADIUS * EXPLOSION_RADIUS )
        {
          if( start == -1 )
          {
            start = j;
            end = j;
          }
          else
            end = j;
        }
    }
    if( start != -1 )
      destroy(i, start, end);
  }
}

void Ground::destroy(int column, int start, int end)
{
  int i;
  for( i = heights[column] - 1; i > end; i-- )
  {
    groudSprites.at(column).pop_back();
    groudSpritesShadow.at(column).pop_back();
    groudSpritesColor.at(column).pop_back();
    world->add(new Pixel(column*sizeSquare.x, window->getSize().y - heights[column] * sizeSquare.y, 0, 0, ImageStorage::getTexture(std::string("pixel")), *world, *window));
    heights[column]--;
  }

  while( i >= start && i >= 0)
  {
    groudSprites.at(column).pop_back();
    groudSpritesShadow.at(column).pop_back();
    groudSpritesColor.at(column).pop_back();
    heights[column]--;
    i--;
  }
}

sf::Rect<float> Ground::getLastCollision()
{
  if( lastI < numberOfSquare && lastI >= 0 )
  {
    sf::Rect<float> boundingBox(lastI * sizeSquare.x, window->getSize().y - heights[lastI] * sizeSquare.y, lengthI * sizeSquare.x, heights[lastI] * sizeSquare.y);
    return boundingBox;
  }
  else
  {
    sf::Rect<float> boundingBox(lastI * sizeSquare.x, window->getSize().y - heights[lastI] * sizeSquare.y, lengthI * sizeSquare.x, heights[lastI] * sizeSquare.y);
    return boundingBox;
  }
}

int Ground::getColumnSize(){
  return numberOfSquare;
}

int* Ground::getHeights(){
  return heights;
}
