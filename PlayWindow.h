#ifndef PLAYWINDOW_H
#define PLAYWINDOW_H

#include <SFML/Graphics.hpp>

class PlayWindow
{
    public:
        PlayWindow(sf::RenderWindow* windowTemp);
        sf::Vector2u getSize();
        sf::Vector2u getRealSize();
        sf::Vector2u getStart();
        void draw(sf::Sprite& drawable, bool game);
        void draw(const sf::Drawable& drawable);
        bool isOpen();
        bool pollEvent(sf::Event& event);
        void close();
        void clear();
        void display();
        sf::RenderWindow* getWindow();

    protected:
        sf::RenderWindow* window;
        sf::View view;
};

#endif
