#ifndef WORLD_H
#define WORLD_H

#include <vector>
#include <SFML/Graphics.hpp>

#include "Ground.h"

class Pixel;
class Character;
class Shot;
class AnimatedText;

class World
{
  public:
  	World();
  	~World();
  	void add(Character* c);
  	void add(Pixel* o);
  	void add(Shot* s);
  	void add(AnimatedText* text);
    void addGround(Ground* groundTemp);
    void emergencyAddToGround(sf::Rect<float> pos, int index);

    void removeText(int i);
    void removeShot(int i);
    void removePixel(int i);
    void removeCharacter(int i);

  	Shot* getShot(int i);
  	AnimatedText* getText(int i);
  	Pixel* getPixel(int i);
  	Character* getCharacter(int i);

    int getNumberShots();
    int getNumberTexts();
    int getNumberCharacters();
    int getNumberPixels();
    int getNoOfColumns();
    int* getHeights();

    void destroyPixels(sf::Rect<float> pos);
    bool checkGroundCollision(sf::Rect<float> pos, int error);
    bool addToGround(sf::Rect<float> pos, int index);
    void drawGround();
    sf::Rect<float> getLastGroundCollision();
    std::vector<AnimatedText*> texts;

  protected:
    std::vector<Character*> characters;
    std::vector<Pixel*> pixels;
    std::vector<Shot*> shots;

    Ground* ground;

};

#endif
