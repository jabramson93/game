#ifndef MUSIX_MIXER_H
#define MUSIX_MIXER_H

#include <SFML/Audio.hpp>

class MusicMixer
{
    public:
        static void setUp();
        static void playLogoMusic();
        static void endLogoMusic();
        static void playDamageMusic();
        static void playShotMusic();
        static void playChangeWeaponMusic();
        static void playBackgroundMusic();

    protected:
        static sf::SoundBuffer damage1, damage2, damage3, laser, weapon;
        static sf::Sound soundDamage1, soundDamage2, soundDamage3, soundLaser, soundWeapon;
        static int damageIndex;
        static sf::Music* logoMusic;
        static sf::Music backgroundMusic;

};

#endif
