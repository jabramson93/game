#ifndef SPLASH_SCREEN_H
#define SPLASH_SCREEN_H

#include <SFML/Graphics.hpp>
#include "Playable.h"
#include "PlayWindow.h"
#include "StartMenu.h"

class SplashScreen : public Playable
{
    public:
        SplashScreen(PlayWindow* windowTemp);
        Playable* play();
        void load();

    protected:
        PlayWindow* window;
        float time;
        int index;
        sf::Clock clock;
        sf::Image image;
        sf::Texture texture;
        sf::Sprite sprite;
        int sizeX, sizeY;
        StartMenu* newStartMenu;
        float totalTime;
};

#endif
