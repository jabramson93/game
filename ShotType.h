#ifndef SHOT_TYPE_H
#define SHOT_TYPE_H

enum ShotType { NORMAL = 0, RICOCHET = 1, DESTRUCTION = 2, REFLECTIVE = 3};

#endif
