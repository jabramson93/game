#include "Background.h"
#include "ImageStorage.h"
#include "Setting.h"
#include "PlayWindow.h"
#include <iostream>

Background::Background(PlayWindow& windowT) : window(windowT)
{
    angle = 0;
    background.setTexture(*ImageStorage::getTexture(std::string("background")));
    background.setScale(Setting::scaling.x, Setting::scaling.y);

    cables.setTexture(*ImageStorage::getTexture(std::string("cables")));
    cables.setScale(Setting::scaling.x, Setting::scaling.y);

    fanLeft.setTexture(*ImageStorage::getTexture(std::string("fan")));
    fanLeft.setOrigin(fanLeft.getGlobalBounds().width/2, fanLeft.getGlobalBounds().height/2);
    //fan.setOrigin( 58, 57);
    fanLeft.setScale(Setting::scaling.x, Setting::scaling.y);
    fanLeft.setPosition(Setting::scaling.x * POSITION_FAN_X_LEFT , Setting::scaling.y * POSITION_FAN_Y_LEFT / 2 );

    fan1Left.setTexture(*ImageStorage::getTexture(std::string("fan")));
    fan1Left.setOrigin(fan1Left.getGlobalBounds().width/2, fan1Left.getGlobalBounds().height/2);
    fan1Left.setScale(Setting::scaling.x, Setting::scaling.y);
    fan1Left.setPosition(Setting::scaling.x * POSITION_FAN_X_LEFT , Setting::scaling.y * POSITION_FAN_Y_LEFT / 2 );

    coolantWireLeft.setTexture(*ImageStorage::getTexture(std::string("coolantWire")));
    coolantWireLeft.setScale(Setting::scaling.x, Setting::scaling.y);
    coolantWireLeft.setPosition(Setting::scaling.x * POSITION_FAN_X_LEFT - coolantWireLeft.getGlobalBounds().width / 2, Setting::scaling.y * 0 );

    coolantBaseLeft.setTexture(*ImageStorage::getTexture(std::string("coolantBase")));
    coolantBaseLeft.setScale(Setting::scaling.x, Setting::scaling.y);
    coolantBaseLeft.setPosition(Setting::scaling.x * POSITION_FAN_X_LEFT - coolantBaseLeft.getGlobalBounds().width / 2, Setting::scaling.y * 0 );


    fanRight.setTexture(*ImageStorage::getTexture(std::string("fan")));
    fanRight.setOrigin(fanRight.getGlobalBounds().width/2, fanRight.getGlobalBounds().height/2);
    fanRight.setScale(Setting::scaling.x, Setting::scaling.y);
    fanRight.setPosition(Setting::scaling.x * POSITION_FAN_X_RIGHT , Setting::scaling.y * POSITION_FAN_Y_RIGHT / 2 );

    fan1Right.setTexture(*ImageStorage::getTexture(std::string("fan")));
    fan1Right.setOrigin(fan1Right.getGlobalBounds().width/2, fan1Right.getGlobalBounds().height/2);
    fan1Right.setScale(Setting::scaling.x, Setting::scaling.y);
    fan1Right.setPosition(Setting::scaling.x * POSITION_FAN_X_RIGHT , Setting::scaling.y * POSITION_FAN_Y_RIGHT / 2 );

    coolantWireRight.setTexture(*ImageStorage::getTexture(std::string("coolantWire")));
    coolantWireRight.setScale(Setting::scaling.x, Setting::scaling.y);
    coolantWireRight.setPosition(Setting::scaling.x * POSITION_FAN_X_RIGHT - coolantWireRight.getGlobalBounds().width / 2, Setting::scaling.y * 0 );

    coolantBaseRight.setTexture(*ImageStorage::getTexture(std::string("coolantBase")));
    coolantBaseRight.setScale(Setting::scaling.x, Setting::scaling.y);
    coolantBaseRight.setPosition(Setting::scaling.x * POSITION_FAN_X_RIGHT - coolantBaseRight.getGlobalBounds().width / 2 , Setting::scaling.y * 0 );

}

void Background::update(float time)
{
    angle += time * ROTATION_SPEED_FAN;
    if( angle > 360 )
        angle -= 360;
    fanLeft.setRotation(angle);
    fan1Left.setRotation(angle+45);
    fanRight.setRotation(angle);
    fan1Right.setRotation(angle+45);
}

void Background::draw()
{
    window.draw(background);
    window.draw(coolantWireLeft);
    window.draw(fan1Left);
    window.draw(fanLeft);
    window.draw(coolantBaseLeft);
    window.draw(coolantWireRight);
    window.draw(fan1Right);
    window.draw(fanRight);
    window.draw(coolantBaseRight);
}

void Background::drawForeground()
{
    window.draw(cables);
}
