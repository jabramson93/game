#ifndef CHARACTER_H
#define CHARACTER_H

#include <SFML/Graphics.hpp>
#include <string>

#include "Object.h"
#include "PlayWindow.h"
#include "ShotType.h"

class Character : public Object
{
  public:
    Character(int X, int Y, int velX, int velY, sf::Texture* texture, World& w, PlayWindow& window, sf::Texture* targetT, int direction, int sideTemp);
    void move( int direction );
    void cancelMove( int direction );
    int update(float timeElapsed );
    void jump();
    void fire(float time, float chargeTime);
    void moveFiringTarget( int direction );
    void cancelMoveFiringTarget( int direction );
    void draw(PlayWindow* window);
    void checkObjectIsInBound(float timeElpased);
    void flip();
    void checkFiringAngle();
    float getXDisplayPosition();
    void takeDamage(int damage);
    int getHealth();
    void nextWeapon();
    void selectWeapon(ShotType i);

    bool collide(float timeElapsed, bool react);
    bool collideShots(float timeElapsed, bool react);
    bool collidePixels(float elapsedTime, bool react);
    bool collideGround(float timeElapsed, bool react);

    sf::FloatRect getBoundingBox();

    void updatePositionAfterGroundCollision(sf::Rect<float> objPos, float elapsedTime);
    sf::Vector2i setRotation( float x, float y);
    void setFiringTarget( float x, float y);
    void updateWeapongImages();
    char* getText();
    void setRechargeTime();

  protected:
    bool takingDamage;
    float damageTime;

    sf::Vector2f dimensionSprite;
    float firingAngle, firingAngleSpeed;
    sf::Texture movingTexture;
    sf::Sprite damageSprite;
    sf::Sprite targetSprite;
    sf::Sprite targetSprite2;
    bool doubleJump;
    bool gripping;
    int health;
    int direction;
    int moveDirection;
    int side;
    float firingTime;
    float armMvtOffset;
    int armMvtDirection;
    ShotType gunType;
    int index, nbFrames;
    float totalTime;
    float rechargeTime;
};

#endif
