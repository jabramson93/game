#include "SplashScreen.h"
#include <SFML/System.hpp>

#include "Playable.h"
#include "Setting.h"
#include "StartMenu.h"
#include "ImageStorage.h"
#include "MusicMixer.h"

SplashScreen::SplashScreen(PlayWindow* windowTemp) : Playable()
{
    MusicMixer::setUp();
    totalTime = 0;
    index = 0;
    sizeX = 640;
    sizeY = 580;
    window = windowTemp;
    image.loadFromFile("logo_sheet.png");
    texture.loadFromImage(image, sf::IntRect(index * sizeX, 0, sizeX, sizeY));
    sprite.setTexture(texture);
    sprite.setScale(Setting::scaling.x, Setting::scaling.y);
    sprite.setPosition(sf::Vector2f(window->getSize().x / 2 - sprite.getGlobalBounds().width / 2,window->getSize().y / 2 - sprite.getGlobalBounds().height / 2 ));
}

void SplashScreen::load()
{
  newStartMenu = StartMenu::getInstance(window);
  newStartMenu->addToRemove(this);

  ImageStorage::loadTexture("Help_Menu.png", std::string("help_menu"));

  ImageStorage::loadTexture("led_off.png", std::string("ledOff"));
  ImageStorage::loadTexture("led_on.png", std::string("ledOn"));

  // Loaded in games.cpp before:
  ImageStorage::loadTexture("character_gripping_sheet.png", std::string("playerGripping"));
  ImageStorage::loadTexture("character_sheet.png", std::string("player"));
  ImageStorage::loadTexture("character_running_sheet.png", std::string("playerRunning"));

  ImageStorage::loadTexture("character_gripping_damage_sheet.png", std::string("playerGrippingDamage"));
  ImageStorage::loadTexture("character_damage_sheet.png", std::string("playerDamage"));
  ImageStorage::loadTexture("character_running_damage_sheet.png", std::string("playerRunningDamage"));

  ImageStorage::loadTexture("weapon_original.png", std::string("weapon_1"));
  ImageStorage::loadTexture("rapidfire.png", std::string("weapon_2"));
  ImageStorage::loadTexture("bazooka.png", std::string("weapon_3"));
  ImageStorage::loadTexture("scoreboard_base_2.png", std::string("scoreboard"));
  ImageStorage::loadTexture("scoreboard_base_2_flipped.png", std::string("scoreboardFlipped"));

  ImageStorage::loadTexture("shot_blue.png", std::string("shotBlue"));
  ImageStorage::loadTexture("shot_green.png", std::string("shotGreen"));
  ImageStorage::loadTexture("shot_red.png", std::string("shotRed"));

  ImageStorage::loadTexture("target.png", std::string("pixel"));
  ImageStorage::loadTexture("target_shadows.png", std::string("pixel_shadow"));
  ImageStorage::loadTexture("weapon_original_background.png", std::string("arm2"));
  ImageStorage::loadTexture("background.png", std::string("background"));
  ImageStorage::loadTexture("cables.png", std::string("cables"));
  ImageStorage::loadTexture("coolant_fan.png", std::string("fan"));
  ImageStorage::loadTexture("coolant_wire.png", std::string("coolantWire"));
  ImageStorage::loadTexture("coolant_base.png", std::string("coolantBase"));
  ImageStorage::loadTexture("scoreboard_healthbar.png", std::string("life"));
}

Playable* SplashScreen::play()
{
    sf::Thread thread(&SplashScreen::load, this);
    thread.launch();
    clock.restart();
    MusicMixer::playLogoMusic();
    while( window->isOpen())
    {
        sf::Event event;
        while (window->pollEvent(event))
        {
            switch (event.type)
            {
                case sf::Event::KeyPressed:
                    if( totalTime > 3 && event.key.code == sf::Keyboard::Escape)
                    {
                        thread.wait();
                        MusicMixer::endLogoMusic();
                        return newStartMenu;
                    }
                    else if( totalTime > 3 && event.key.code == sf::Keyboard::Space)
                    {
                        thread.wait();
                        MusicMixer::endLogoMusic();
                        return newStartMenu;
                    }
                    break;

                case sf::Event::JoystickButtonPressed:
                    if( totalTime > 3 && event.joystickButton.button == 0)
                    {
                        thread.wait();
                        MusicMixer::endLogoMusic();
                        return newStartMenu;
                    }
                    else if ( totalTime > 3 && event.joystickButton.button == 1 )
                    {
                        thread.wait();
                        MusicMixer::endLogoMusic();
                        return newStartMenu;
                    }
                    break;
            }
        }
        time += clock.getElapsedTime().asSeconds();
        if( time > 0.075 )
        {
            index++;
            if( index > 15 )
                index = 0;
            texture.loadFromImage(image, sf::IntRect(index * sizeX, 0, sizeX, sizeY));
            totalTime += time;
            time = 0;
        }
        if( totalTime > 8 )
        {
            return StartMenu::getInstance(window);
        }
        clock.restart();
        window->clear();
        window->draw(sprite);
        window->display();
    }
    thread.wait();
    return NULL;

}
