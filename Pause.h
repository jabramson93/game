#ifndef PAUSE_H
#define PAUSE_H

#include "Game.h"
#include "Playable.h"
#include "PlayWindow.h"

class Pause : public Playable
{
    public:
        Pause(PlayWindow* windowTemp, Game* gameTemp);
        int keyboardEvents();
        void drawPause();
        Playable* play();
        void updateColorText();
        void xboxController(int i);
        void increment();
        void decrement();


    protected:
        PlayWindow* window;
        Game* game;
        sf::Font fontPause;
        sf::Text textPaused, textPlayNewGame, textResume, textGoBack, textExit;
        int selectedItem;
        bool moving[2];

};

#endif // PAUSE_H
