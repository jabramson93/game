#ifndef PLAYABLE_H
#define PLAYABLE_H

class Playable
{
    public:
        Playable();
        virtual ~Playable();
        virtual Playable* play();
        Playable* addToRemove(Playable* toRemove);

    protected:
        Playable* toRemove;
};

#endif
