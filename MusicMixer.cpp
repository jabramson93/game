#include "MusicMixer.h"

sf::SoundBuffer MusicMixer::damage1, MusicMixer::damage2, MusicMixer::damage3, MusicMixer::laser, MusicMixer::weapon;
sf::Sound MusicMixer::soundDamage1, MusicMixer::soundDamage2, MusicMixer::soundDamage3, MusicMixer::soundLaser, MusicMixer::soundWeapon;
sf::Music* MusicMixer::logoMusic;
sf::Music MusicMixer::backgroundMusic;
int MusicMixer::damageIndex;

void MusicMixer::setUp()
{
    logoMusic = new sf::Music();
    logoMusic->openFromFile("sound/dumb.wav");

    backgroundMusic.openFromFile("sound/minecraft_forest_by_NubbyCakums.wav");

    damage1.loadFromFile("sound/hurt1.wav");
    damage2.loadFromFile("sound/hurt2.wav");
    damage3.loadFromFile("sound/hurt3.wav");
    laser.loadFromFile("sound/laser_sound.wav");
    weapon.loadFromFile("sound/change_weapon.wav");

    soundDamage1.setBuffer(damage1);
    soundDamage2.setBuffer(damage2);
    soundDamage3.setBuffer(damage3);
    soundLaser.setBuffer(laser);
    soundWeapon.setBuffer(weapon);

    damageIndex = 0;
}

void MusicMixer::playBackgroundMusic()
{
    if( backgroundMusic.getStatus() != sf::Sound::Playing )
    {
        backgroundMusic.setLoop(true);
        backgroundMusic.play();
    }
}

void MusicMixer::playLogoMusic()
{
    logoMusic->play();
}

void MusicMixer::endLogoMusic()
{
    logoMusic->stop();
    free(logoMusic);
}

void MusicMixer::playShotMusic()
{
    soundLaser.play();
}

void MusicMixer::playChangeWeaponMusic()
{
    soundWeapon.play();
}

void MusicMixer::playDamageMusic()
{
    if( damageIndex == 0 )
        soundDamage1.play();
    if( damageIndex == 1 )
        soundDamage2.play();
    if( damageIndex == 2 )
        soundDamage3.play();
    damageIndex++;
    if( damageIndex > 2 )
        damageIndex = 0;
}
