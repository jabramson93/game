#include "Pixel.h"
#include "Character.h"
#include <iostream>
#include <stdlib.h>
#include "Setting.h"
#include "PlayWindow.h"

Pixel::Pixel( int X, int Y, int velX, int velY, sf::Texture* texture, World& w, PlayWindow& window ) : Object( X, Y, velX, velY, texture, w, window)
{
  creationTime = 0;
  sprite.scale(0.5, 0.5);
  dimension.x = sprite.getGlobalBounds().width;
  dimension.y = sprite.getGlobalBounds().height;
}

int Pixel::update(float elapsedTime, int index)
{
  creationTime += elapsedTime;
  if( creationTime > PIXEL_LIFE )
  {
    world.emergencyAddToGround(getBoundingBox(), index);
    return -1;
  }

  velocity.y += gravity * elapsedTime;
  //velocity.x -= elapsedTime * velocity.x / 100;
  if( velocity.y * velocity.y + velocity.x * velocity.x > SQUARED_MAXIMUM_PIXEL_VELOCITY )
    velocity.y -= gravity * elapsedTime;

  lastPosition = position;
  position += velocity * elapsedTime;
  sprite.setPosition(position);
  if( world.addToGround(getBoundingBox(), index) )
    return -1;
  collide(elapsedTime, true);
  checkObjectIsInBound(elapsedTime);
  sprite.setPosition(position);
  return 1;
}

bool Pixel::collide(float elapsedTime, bool react)
{
  collidePixels(elapsedTime);
  collideGround(elapsedTime);
}

void Pixel::collideGround(float elapsedTime)
{
  if( world.checkGroundCollision(getBoundingBox(), 1) )
  {
    sf::Rect<float> gPos = world.getLastGroundCollision();
    updatePositionAfterGroundCollision(gPos, elapsedTime);
    if( world.checkGroundCollision(getBoundingBox(), 1) )
    {
      position = lastPosition;
      velocity.x = 0;
      velocity.y = 0;
    }
  }
}

void Pixel::updatePositionAfterGroundCollision(sf::Rect<float> objPos, float elapsedTime)
{
  sf::Vector2f oldPosition = lastPosition;
  if( velocity.x == 0 || !( oldPosition.x + dimension.x < objPos.left || oldPosition.x > objPos.left + objPos.width ) )
  {
    velocity.y = 0;
    position.y = oldPosition.y;
  }
  else if( velocity.y == 0 || !( oldPosition.y + dimension.y < objPos.top || oldPosition.y > objPos.top + objPos.height ) )
  {
    /*if ( velocity.x < 0 )
      position.x = objPos.left + objPos.width+1;
    else
      position.x = objPos.left - dimension.x-1;*/
    position.x = oldPosition.x;
    velocity.x = 0;
  }
  else
  {
    position = lastPosition;
    velocity.x = 0;
    velocity.y = 0;
  }
  sprite.setPosition(position);
}

void Pixel::addVelocity(sf::Vector2f velocityToAdd)
{
    Object::addVelocity(velocityToAdd);
    if( velocity.y * velocity.y + velocity.x * velocity.x > SQUARED_MAXIMUM_PIXEL_VELOCITY )
        Object::addVelocity(-velocityToAdd);
}

void Pixel::collidePixels(float elapsedTime)
{
  bool collisionOccured = false;
  // Test collisions with characters:
  for( int i = 0; i < world.getNumberPixels() ; i++ )
    if( this != world.getPixel(i)
        && collision((Object*)world.getPixel(i) ))
    {
      if( !collision((Object*)world.getPixel(i) ) )
        continue;
      if( collisionState == 1 || world.getPixel(i)->collisionState == 1)
      {
        sf::Vector2f addedVel(velocity);
        addedVel.x /= 10;
        addedVel.y /= 5;
        world.getPixel(i)->addVelocity(addedVel);
        updatePositionAfterCollision(world.getPixel(i), elapsedTime);
      }
      else
      {
        collisionOccured = true;
      }
    }
  if( !collisionOccured  )
    collisionState = 1;
}

void Pixel::collideCharacters(float elapsedTime)
{
  // Test collisions with characters:
  for( int i = 0; i < world.getNumberCharacters() ; i++ )
    if( collision((Object*)world.getCharacter(i) ))
    {
      reactCollisionCharacter(world.getCharacter(i));
      world.getCharacter(i)->reactCollisionPixel(this);
      updatePositionAfterCollision(world.getCharacter(i), elapsedTime);
    }
}
