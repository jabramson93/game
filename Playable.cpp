#include "Playable.h"
#include <cstddef>

Playable::Playable()
{
    toRemove = NULL;
}


Playable::~Playable()
{

}

Playable* Playable::addToRemove(Playable* toRemoveTemp)
{
    toRemove = toRemoveTemp;
}

Playable* Playable::play()
{
    if( toRemove != NULL )
        delete toRemove;
    toRemove = NULL;
}
