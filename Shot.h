#ifndef SHOT_H
#define SHOT_H

#include <string>

#include "Object.h"
#include "ShotType.h"
#include "PlayWindow.h"

class Shot: public Object
{
  public:
    Shot(int X, int Y, int velX, int velY, sf::Texture* texture, World& w, PlayWindow& window, float firingAngle, Object* parentT, float chargeTimeTemp, ShotType typeTemp, int side);
    int update(float elapsedTime);
    void reactCollision();
    Object* parent;
    void callReactCollision(Object* o);
    void collideCharacters();
    void collideGround();
    void bounce(sf::Rect<float> box);

    bool collide(float elapsedTime, bool react);
    void reactCollision(sf::Vector2f meanVelocity, float dispersion);

    sf::Vector2f getWallCollisionVector();

    void reactCollisionPixel(Object* p);
    void reactCollisionShot(Shot* s);
    void reactCollisionCharacter(Character* c);
    void reactWallCollision();

    sf::FloatRect getBoundingBox();

    void checkObjectIsInBound( float timeElpased);

    void collideShots();
    ShotType type;

  protected:
    float angle;
    int state;
    int numberOfPixels;
    float timeElapsed;
    int frame;
    int bounces;
    sf::Texture* textures[2];

};

#endif
