#ifndef IMAGESTORAGE_H
#define IMAGESTORAGE_H

#include <string>
#include <map>
#include <SFML/Graphics.hpp>

class ImageStorage
{
	public:
	  static void loadTexture(char* name, std::string reference);
	  static sf::Texture* getTexture(std::string reference);

	private:
	  static std::map<std::string, sf::Texture*> mapToTextures;
};

#endif