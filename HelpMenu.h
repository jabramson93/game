
#ifndef HELPMENU_H
#define HELPMENU_H

#include <SFML/Graphics.hpp>
#include "Playable.h"
#include "PlayWindow.h"


class HelpMenu : public Playable
{
    public:
        HelpMenu(PlayWindow* windowTemp);
        Playable* play();

    protected:
        Playable* menu;
        PlayWindow* window;
        sf::Texture texture, textureButton;
        sf::Sprite sprite, spriteButton;

};

#endif // HELPMENU_H

