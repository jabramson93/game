#include "WinScreen.h"

#include <iostream>
#include <sstream>

#include "Setting.h"
#include "StartMenu.h"
#include "PlayWindow.h"
#include "Game.h"
#include "ImageStorage.h"


WinScreen::WinScreen(PlayWindow* windowTemp, Playable* previous,int player1NumberWinsTemp, int player2NumberWinsTemp) : Playable()
{
  if( previous != NULL )
    delete(previous);

  offset = 0;
  index = 0;
  player1NumberWins = player1NumberWinsTemp;
  player2NumberWins = player2NumberWinsTemp;

  texture.loadFromFile("start_background.gif");
  sprite.setTexture(texture);
  sprite.setScale(Setting::scaling.x, Setting::scaling.y);

  image.loadFromFile("start_background.png");
  numberFrames = image.getSize().x / 1366;
  sprites = (sf::Sprite*)malloc(sizeof(sf::Sprite) * numberFrames);
  textureBackground.loadFromImage(image, sf::IntRect(offset * 1366, 0, 1366, 768));
  for( int i = 0 ; i < numberFrames - 1; i++ )
  {
      // texture.loadFromImage(image, sf::IntRect(i * 1366, 0, 1366, 768));
      sprites[i].setTexture(texture);
  }
  textureBackground.loadFromImage(image, sf::IntRect(offset * 1366, 0, 1366, 768));
  spriteBackground.setTexture(texture);
  spriteBackground.setScale(Setting::scaling.x, Setting::scaling.y);

  textureChar.loadFromFile("character_thrusting_sheet.png");
  spriteChar.setTexture(textureChar);
  spriteChar.setScale(Setting::scaling.x, Setting::scaling.y);
  spriteChar.setOrigin(90,40);

  window = windowTemp;

    if( player1NumberWins > player2NumberWins ){
        textureText.loadFromFile("player1_win.png");
    }
    else if ( player1NumberWins < player2NumberWins)
        textureText.loadFromFile("player2_win.png");
    else if(player1NumberWinsTemp == player2NumberWinsTemp)
        textureText.loadFromFile("text/tie.png");


  spriteText.setTexture(textureText);
  spriteChar.setPosition(window->getSize().x / 2 + 15 * Setting::scaling.x, window->getSize().y / 2 + 60 * Setting::scaling.y);
  spriteText.setPosition( window->getSize().x / 2 - spriteText.getGlobalBounds().width / 2, window->getSize().y / 2 - spriteText.getGlobalBounds().height / 2 );
}



Playable* WinScreen::play()
{
  timeElapsed = 0;
  while( window->isOpen() )
  {
    sf::Event event;
    while (window->pollEvent(event))
    {
        switch (event.type)
        {
            case sf::Event::KeyPressed:
                if( event.key.code == sf::Keyboard::Escape)
                    window->close();
                else if( event.key.code == sf::Keyboard::Return)
                {
                        //delete previous;
                        StartMenu::getInstance(window)->addToRemove(this);
                        return StartMenu::getInstance(window);
                }
                break;

            case sf::Event::JoystickButtonPressed:
                if( event.joystickButton.button == 0)
                {
                    //delete previous;
                    StartMenu::getInstance(window)->addToRemove(this);
                    return StartMenu::getInstance(window);
                }
                break;
        }
    }


    timeElapsed += clock.getElapsedTime().asSeconds();
    clock.restart();

    if( timeElapsed > START_MENU_ANIMATION_SPEED )
    {
        timeElapsed = 0;
        offset++;
        if( offset > numberFrames - 1 )
            offset = 0;
        texture.loadFromImage(image, sf::IntRect(offset * 1366, 0, 1366, 768));
        if(index == 0 )
            index=1;
        else
            index=0;
    }


    dimension.x = ImageStorage::getTexture(std::string("player"))->getSize().x / 5;
    dimension.y = ImageStorage::getTexture(std::string("player"))->getSize().y;
    spriteChar.setTextureRect(sf::IntRect(index * 180, 0, 180, 160));


    if( window->isOpen() )
    {
      window->clear();
      window->draw(spriteBackground);
      window->draw(spriteText);
      window->draw(spriteChar);
      window->display();
    }
  }
    return NULL;

}








