#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>
#include <math.h>

#include "Object.h"
#include "Setting.h"
#include "PlayWindow.h"

Object::Object(int X, int Y, int velX, int velY, sf::Texture* texture, World& w, PlayWindow& windowT ) :  world(w), window(windowT)
{
  position.x = X;
  position.y = Y;
  velocity.x = velX;
  velocity.y = velY;
  sprite.setTexture(*texture);
  sprite.setPosition(position);
  sprite.setScale(sf::Vector2f(Setting::scaling.x, Setting::scaling.y));
  gravity = GRAVITY * Setting::scaling.y;
  collisionState = -1;
  bouncing = true;
  dimension.x = sprite.getGlobalBounds().width;
  dimension.y = sprite.getGlobalBounds().height;
}

void Object::draw(PlayWindow* window)
{
  sprite.setPosition(position);
  window->draw(sprite, true);
}

int Object::update(float elapsedTime)
{
  velocity.y += gravity * elapsedTime;
  lastPosition = position;
  position += velocity * elapsedTime;
  sprite.setPosition(position);
  collide(elapsedTime, true);
  checkObjectIsInBound(elapsedTime);
  sprite.setPosition(position);
  return 1;
}

void Object::setVelocity(sf::Vector2f newVelocity)
{
  velocity.y = newVelocity.y * Setting::scaling.y;
}

void Object::updatePositionAfterCollision(Object* collideWith, float elapsedTime)
{
  sf::Vector2f oldPosition = lastPosition;
  if( !bouncing )
  {
    if( !( oldPosition.x + dimension.x < collideWith->position.x || oldPosition.x > collideWith->position.x + collideWith->dimension.y ) )
    {
      velocity.y = 0;
      position.y = lastPosition.y;
    }
    else if( !( oldPosition.y + dimension.y < collideWith->position.y || oldPosition.y > collideWith->position.y + collideWith->dimension.y ) )
    {
      velocity.x = 0;
      position.x = lastPosition.x;
    }
    else
    {
      velocity.x = 0;
      velocity.y = 0;
      sprite.setPosition(position);
    }
    return;
  }
  if( !( oldPosition.x + dimension.x < collideWith->position.x || oldPosition.x > collideWith->position.x + collideWith->dimension.x ) )
  {
    velocity.y = -velocity.y / 2;
    position.y = lastPosition.y;
  }
  else if( !( oldPosition.y + dimension.y < collideWith->position.y || oldPosition.y > collideWith->position.y + collideWith->dimension.y ) )
  {
    velocity.x = -velocity.x / 2;
    position.x = lastPosition.x;
  }
  else
  {
    velocity.x = -velocity.x / 2;
    velocity.y = -velocity.y / 2;
    position = lastPosition;
  }
  // position = lastPosition;
  sprite.setPosition(position);
}

void Object::checkObjectIsInBound( float timeElpased)
{
  if( position.y + dimension.y > window.getSize().y )
  {
    if( bouncing )
      velocity.y = - 0.5 * velocity.y;
    else
      velocity.y = 0;
    position.y = window.getSize().y - dimension.y;
    reactWallCollision();
  }
  else if ( position.y < 0 )
  {
    if( bouncing )
      velocity.y = - 0.5 * velocity.y;
    else
      velocity.y = 0;
    position.y = 0;
    reactWallCollision();
  }

  if( position.x + dimension.x > window.getSize().x )
  {
    if( bouncing )
      velocity.x = - 0.5 * velocity.x;
    else if( velocity.y > 0 )
    {
      velocity.y -= WALL_GRIPPING_VELOCITY * timeElpased;
      if( velocity.y < 0)
        velocity.y = 0;
    }
    position.x = window.getSize().x - dimension.x;
    reactWallCollision();
  }
  else if ( position.x < 0 )
  {
    if( bouncing )
      velocity.x = - 0.5 * velocity.x;
    else if( velocity.y > 0 )
    {
      velocity.y -= WALL_GRIPPING_VELOCITY * timeElpased;
      if( velocity.y < 0)
        velocity.y = 0;
    }
    position.x = 0;
    reactWallCollision();
  }
}

sf::FloatRect Object::getBoundingBox()
{
    return sprite.getGlobalBounds();
}

bool Object::isOnGround()
{
  bool response = false;
  position.y += 1 * Setting::scaling.y;
  sprite.setPosition(position);
  if( world.checkGroundCollision(this->getBoundingBox(), 0) )
    response = true;
  position.y -= 1 * Setting::scaling.y;
  sprite.setPosition(position);
  return response;
}

void Object::setVelocityX(float velX)
{
  velocity.x = velX;
}

void Object::addVelocity(sf::Vector2f velocityToAdd)
{
  velocity.x += velocityToAdd.x * Setting::scaling.x;
  velocity.y += velocityToAdd.y * Setting::scaling.y;
}

bool Object::collision(Object* object)
{
  return this->getBoundingBox().intersects(object->getBoundingBox());
}

void Object::reactCollisionPixel(Pixel* p)
{
}

void Object::reactWallCollision()
{
}

void Object::reactCollisionShot(Shot* s)
{
}

void Object::reactCollisionCharacter(Character* c)
{
}
