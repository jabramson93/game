#ifndef STARTMENU_H
#define STARTMENU_H

#include <SFML/Graphics.hpp>

#include "Playable.h"
#include "PlayWindow.h"

class StartMenu : public Playable
{
    public:
        static StartMenu* getInstance(PlayWindow* window);
        Playable* play();
        void updateColorText();
        void xboxController(int i);
        void increment();
        void decrement();

    protected:
        sf::Vector2f dimensionSprite;
        StartMenu(PlayWindow* windowTemp, Playable* previous);
        static StartMenu* startMenu;
        PlayWindow* window;
        sf::Font font;
        sf::Text playText, creditsText, exitText, helpText;
        int selectedItem;
        int counter;
        float rotateSwitch;
        int switcher;
        bool moving[2];
        sf::Sprite sprite;
        sf::Sprite* sprites;
        sf::Texture texture;
        sf::Sprite spriteLogo, spriteChar, spriteArm, spriteButton;
        sf::Texture textureLogo, textureChar, textureArm, textureButton;
        sf::Image image;
        sf::Clock clock;
        float timeElapsed;
        int offset, index;
        float rotation;
        int numberFrames;
};

#endif
