#ifdef __WIN32__
  //#include "X360Controller.hpp"
#endif


#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <cmath>

#include "Game.h"
#include "ImageStorage.h"
#include "Character.h"
#include "Shot.h"
#include "Pixel.h"
#include "Setting.h"
#include "Ground.h"
#include "Pause.h"
#include "StartMenu.h"
#include "PlayWindow.h"

Pause::Pause(PlayWindow* windowTemp, Game* gameTemp) : Playable()
{
    game = gameTemp;
    window = windowTemp;
    selectedItem = 0;
    fontPause.loadFromFile("Minecraftia.ttf");

    textPaused.setFont(fontPause);
    textPaused.setCharacterSize(30 * Setting::scaling.y);
    textPaused.setString("Paused");
    textPaused.setColor(sf::Color(100, 100, 100, 255));
    textPaused.setPosition( window->getRealSize().x / 2 - textPaused.getGlobalBounds().width / 2, window->getRealSize().y / 2 - textPaused.getGlobalBounds().height / 2 - 80 * Setting::scaling.y);

    textResume.setFont(fontPause);
    textResume.setCharacterSize(24 * Setting::scaling.y);
    textResume.setString("Resume");
    textResume.setPosition( window->getRealSize().x / 2 - textResume.getGlobalBounds().width / 2,  window->getRealSize().y / 2 - textResume.getGlobalBounds().height / 2 - 40 * Setting::scaling.y);

    textPlayNewGame.setFont(fontPause);
    textPlayNewGame.setCharacterSize(24 * Setting::scaling.y);
    textPlayNewGame.setString("Play Again");
    textPlayNewGame.setPosition( window->getRealSize().x / 2 - textPlayNewGame.getGlobalBounds().width / 2, window->getRealSize().y / 2 - textPlayNewGame.getGlobalBounds().height / 2 );

    textGoBack.setFont(fontPause);
    textGoBack.setCharacterSize(24 * Setting::scaling.y);
    textGoBack.setString("Back to menu");
    textGoBack.setPosition( window->getRealSize().x / 2 - textGoBack.getGlobalBounds().width / 2, window->getRealSize().y / 2 - textGoBack.getGlobalBounds().height / 2 + 40 * Setting::scaling.y);

    textExit.setFont(fontPause);
    textExit.setCharacterSize(24 * Setting::scaling.y);
    textExit.setString("Exit Game");
    textExit.setPosition( window->getRealSize().x / 2 - textExit.getGlobalBounds().width / 2, window->getRealSize().y / 2 - textExit.getGlobalBounds().height / 2 + 80 * Setting::scaling.y);
    updateColorText();
}

Playable* Pause::play()
{
    while(window->isOpen())
    {
        xboxController(0);
        xboxController(1);
        if( keyboardEvents() == -1 )
        {
            if( selectedItem == 0)
            {
                game->pause();
                game->addToRemove(this);
                return game;
            }
            else if ( selectedItem == 1)
            {
                delete(game);
                return new Game(window, this);
            }
            else if ( selectedItem == 2)
            {
                delete(game);
                StartMenu::getInstance(window)->addToRemove(this);
                return StartMenu::getInstance(window);
            }
            else if ( selectedItem == 3)
            {
                delete(game);
                window->close();
                return NULL;
            }
        }
        drawPause();
    }
    return NULL;
}

void Pause::drawPause()
{
    window->clear();
    game->draw();

    sf::RectangleShape black(sf::Vector2f(window->getRealSize().x, window->getRealSize().y) );
    black.setFillColor(sf::Color(0,0,0,200));
    window->draw(black);

    window->draw(textPaused);
    window->draw(textResume);
    window->draw(textPlayNewGame);
    window->draw(textGoBack);
    window->draw(textExit);

    window->display();
}

int Pause::keyboardEvents()
{
    sf::Event event;
    while (window->pollEvent(event))
    {
        switch (event.type)
        {
            case sf::Event::KeyPressed:
                if(event.key.code == sf::Keyboard::V)
                {
                    selectedItem = 0;
                    return -1;
                }
                else if( event.key.code == sf::Keyboard::Escape)
                    window->close();
                else if( event.key.code == sf::Keyboard::Return)
                    return -1;
                else if( event.key.code == sf::Keyboard::Down)
                   increment();
                else if( event.key.code == sf::Keyboard::Up)
                    decrement();
                break;

            case sf::Event::JoystickButtonPressed:
                if( event.joystickButton.button == 0)
                {
                    return -1;
                }
                else if( event.joystickButton.button == 1)
                {
                    selectedItem = 0;
                    return -1;
                }
                break;
        }
    }
    return 1;
}

void Pause::updateColorText()
{
    if( selectedItem == 0)
    {
        textResume.setColor(sf::Color(255, 255, 0, 255));
        textPlayNewGame.setColor(sf::Color(255, 255, 255, 255));
        textGoBack.setColor(sf::Color(255, 255, 255, 255));
        textExit.setColor(sf::Color(255, 255, 255, 255));
    }
    else if ( selectedItem == 1 )
    {
        textResume.setColor(sf::Color(255, 255, 255, 255));
        textPlayNewGame.setColor(sf::Color(255, 255, 0, 255));
        textGoBack.setColor(sf::Color(255, 255, 255, 255));
        textExit.setColor(sf::Color(255, 255, 255, 255));
    }
    else if ( selectedItem == 2 )
    {
        textResume.setColor(sf::Color(255, 255, 255, 255));
        textPlayNewGame.setColor(sf::Color(255, 255, 255, 255));
        textGoBack.setColor(sf::Color(255, 255, 0, 255));
        textExit.setColor(sf::Color(255, 255, 255, 255));
    }
    else if ( selectedItem == 3 )
    {
        textResume.setColor(sf::Color(255, 255, 255, 255));
        textPlayNewGame.setColor(sf::Color(255, 255, 255, 255));
        textGoBack.setColor(sf::Color(255, 255, 255, 255));
        textExit.setColor(sf::Color(255, 255, 0, 255));
    }
}

void Pause::increment()
{
    selectedItem++;
    if( selectedItem > 3 )
        selectedItem = 3;
    updateColorText();
}

void Pause::decrement()
{
    selectedItem--;
    if( selectedItem < 0 )
        selectedItem = 0;
    updateColorText();
}

void Pause::xboxController(int i)
{
    if( sf::Joystick::getAxisPosition(i, sf::Joystick::Y) > 80 )
    {
        if( !moving[i] )
            increment();
        moving[i] = true;
    }
    else if( sf::Joystick::getAxisPosition(i, sf::Joystick::Y) < -80 )
    {
        if( !moving[i] )
            decrement();
        moving[i] = true;
    }
    else if ( moving[i] )
    {
        moving[i] = false;
    }
}
