#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <SFML/Graphics.hpp>
#include "PlayWindow.h"

class Background
{
    public:
        Background(PlayWindow& windowT);
        void update(float time);
        void draw();
        void drawForeground();

    protected:
        sf::Sprite background, coolantBaseLeft, coolantWireLeft, fanLeft, fan1Left, coolantBaseRight, coolantWireRight, fanRight, fan1Right, cables;
        PlayWindow& window;
        float angle;
};


#endif
