#include "AnimatedText.h"
#include <iostream>

AnimatedText::AnimatedText(PlayWindow* windowTemp, char* textChar, sf::Vector2f pos)
{
    window = windowTemp;
    font.loadFromFile("Minecraftia.ttf");
    text.setFont(font);
    text.setString(textChar);
    text.setColor(sf::Color(255, 255, 255, 255));
    text.setPosition(pos);
    acc = 0;
}

int AnimatedText::update(float time)
{
    text.move(0, - time * 80);
    acc += time * 150;
    text.setColor(sf::Color(255, 255, 255, 255 - acc));
    if( acc > 255)
        return -1;
    return 0;
}

void AnimatedText::draw(PlayWindow* window)
{
    window->draw(text);
}
