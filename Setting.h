#ifndef SETTING_H
#define SETTING_H

#include <SFML/Graphics.hpp>

// Define constant for the game:

// Arm positioning and origin of rotation
#define RIGHT_ARM_OFFSET_X 44
#define RIGHT_ARM_OFFSET_Y 48
#define ARM_ORIGIN_X 8
#define ARM_ORIGIN_Y 20

// Arm positioning and origin of rotation
#define LEFT_ARM_OFFSET_X 44
#define LEFT_ARM_OFFSET_Y 48


// Variable related to jump and fall:
#define GRAVITY 1500
#define SPEED_FIRST_JUMP -1000
#define SPEED_SECOND_JUMP -750

// Others:
#define RECHARGE_TIME 0.4
#define DAMAGE 3.75

#define CHARGE_COEFFICIENT 10
#define MAXIMUM_NUMBER_PIXELS_PER_SHOT 50

#define MAXIMUM_NUMBER_PIXEL_CLIMBED 3
#define CHARACTER_SPEED 500
#define MOUSE_DISTANCE 100

#define ARM_OFFSET_SPEED 1
#define MAXIMUM_ARM_MOVMENT_OFFSET 2

#define WALL_GRIPPING_VELOCITY 10000

#define PIXEL_SIZE 20
#define ORIGINAL_PIXEL_SIZE 20
#define PIXEL_SPEED 100
#define PIXEL_LIFE 5

#define SHOT_BOX_SIZE_X 20
#define SHOT_BOX_SIZE_Y 20

#define SHOT_SPEED 1000

#define SQUARED_MAXIMUM_PIXEL_VELOCITY 1000000
#define LIMIT_X_SPEED 100

#define FIRING_ANGLE_SPEED 100

#define SCORE_BOARD1_X 252
#define SCORE_BOARD1_Y 24

#define SCORE_BOARD2_X 760
#define SCORE_BOARD2_Y 24

#define LED_LEFT_X SCORE_BOARD1_X + 77
#define LED_LEFT_Y SCORE_BOARD1_Y + 113

#define LED_RIGHT_X SCORE_BOARD2_X + 354 - 77 - 23 * 3 - 5 * 2
#define LED_RIGHT_Y SCORE_BOARD2_Y + 113

#define HEALTH1_X_POS SCORE_BOARD1_X + 105
#define HEALTH1_Y_POS SCORE_BOARD1_Y + 81

#define HEALTH2_X_POS SCORE_BOARD2_X + 42
#define HEALTH2_Y_POS SCORE_BOARD2_Y + 81

#define SCORE1_X_POS SCORE_BOARD1_X + 44
#define SCORE1_Y_POS SCORE_BOARD1_Y + 52

#define SCORE2_X_POS SCORE_BOARD2_X + 44
#define SCORE2_Y_POS SCORE_BOARD2_Y + 52

#define SHOT_ANIMATION_TIME 0.1

#define SCORE_CONSTANT 10

#define NUMBER_OF_BOUNCES 2
#define EXPLOSION_RADIUS 50

#define ROTATION_SPEED_FAN 400
#define POSITION_FAN_X_LEFT 156
#define POSITION_FAN_Y_LEFT 169
#define POSITION_FAN_X_RIGHT 1206
#define POSITION_FAN_Y_RIGHT 169

#define POSITION_TIMER_Y 10

#define MINIMUM_TIME_BETWEEN_FRAME 0.016666666666666

#define CHARACTER_SIZE_X 450
#define CHARACTER_SIZE_Y 120
#define TIME_BY_FRAME 0.2
#define TIME_BY_FRAME_RUNNING 0.1

#define START_MENU_ANIMATION_SPEED 0.15

class Setting
{
  public:
    static sf::Vector2f scaling;
    static bool xboxControllerAllowed;
    static bool xboxControllerActivated;
};

#endif
