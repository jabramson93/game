#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <cmath>
#include <stdlib.h>
#include <time.h>

#include "Game.h"
#include "AnimatedText.h"
#include "ImageStorage.h"
#include "Character.h"
#include "Shot.h"
#include "Pixel.h"
#include "Setting.h"
#include "Ground.h"
#include "Pause.h"
#include "EndMenu.h"
#include "PlayWindow.h"


Game::Game(PlayWindow* windowTemp, Playable* previous, int roundTemp, int player1NumberWinsTemp, int player2NumberWinsTemp) : Playable(), world(), timerDisplay(windowTemp)
{
  round = roundTemp;
  player1NumberWins = player1NumberWinsTemp;
  player2NumberWins = player2NumberWinsTemp;

  window = windowTemp;
  srand (0);

  background = new Background(*windowTemp);

  world.addGround(new Ground( ceil(window->getSize().x / ( PIXEL_SIZE * Setting::scaling.x ) ) + 1, ceil(window->getSize().y / ( PIXEL_SIZE * Setting::scaling.y ) ) + 1, sf::Vector2f( PIXEL_SIZE * Setting::scaling.x, PIXEL_SIZE * Setting::scaling.y), window, &world));

  paused = false;

  fontFPS.loadFromFile("Minecraftia.ttf");

  setText(textFPS, 10, 10);
  setText(textScore1, Setting::scaling.x * SCORE1_X_POS, Setting::scaling.y * SCORE1_Y_POS);
  setText(textScore2, Setting::scaling.x * SCORE2_X_POS, Setting::scaling.y * SCORE2_Y_POS );
  textScore1.setColor(sf::Color(91, 116, 35, 255));
  textScore2.setColor(sf::Color(91, 116, 35, 255));

  textTimer.setFont(fontFPS);
  //textTimer.setCharacterSize(40 * Setting::scaling.x);
  textTimer.setColor(sf::Color::White);

  timer = 0;

  for( int i =0; i < 2; i++)
  {
      readyToJump[i] = true;
      weaponCharging[i] = false;
  }

  scoreBase1.setTexture(*ImageStorage::getTexture(std::string("scoreboard")));
  scoreBase1.setPosition(sf::Vector2f (Setting::scaling.x * SCORE_BOARD1_X, Setting::scaling.y * SCORE_BOARD1_Y));

  scoreBase2.setTexture(*ImageStorage::getTexture(std::string("scoreboardFlipped")));
  scoreBase2.setPosition(sf::Vector2f (Setting::scaling.x * SCORE_BOARD2_X, Setting::scaling.y * SCORE_BOARD2_Y));

  health1.setTexture(*ImageStorage::getTexture(std::string("life")));
  health1.setPosition(sf::Vector2f (Setting::scaling.x * HEALTH1_X_POS, Setting::scaling.y * HEALTH1_Y_POS));

  health2.setTexture(*ImageStorage::getTexture(std::string("life")));
  health2.setPosition(sf::Vector2f (Setting::scaling.x * HEALTH2_X_POS, Setting::scaling.y * HEALTH2_Y_POS));

  //Create the players:
  float quaterScreen = window->getSize().x / 4;
  float halfPersoSizeX = ImageStorage::getTexture(std::string("player"))->getSize().x / 5 * Setting::scaling.x / 2;
  world.add(new Character(quaterScreen - halfPersoSizeX, window->getSize().y - 1 - ImageStorage::getTexture(std::string("player"))->getSize().y * Setting::scaling.y, 0, 0, ImageStorage::getTexture(std::string("player")), world, *window, ImageStorage::getTexture(std::string("weapon_1")), 1, 1));
  world.add(new Character(3 * quaterScreen - halfPersoSizeX, window->getSize().y - 1 - ImageStorage::getTexture(std::string("player"))->getSize().y * Setting::scaling.y, 0, 0, ImageStorage::getTexture(std::string("player")), world, *window, ImageStorage::getTexture(std::string("weapon_1")), -1, 2));
  setUpLeds();
}

void Game::setText(sf::Text& text, float positonx, float positionY)
{
  text.setFont(fontFPS);
  text.setCharacterSize(24);
  text.setPosition(positonx, positionY);
  text.setColor(sf::Color::White);
}

Playable* Game::play()
{
  if( timer == 0 )
    startAnimation();
  Playable::play();
  sf::Clock clock;
  int frameCounter =  0;
  float timeElapsedFPS = 0;
  float fps = 0;

  while (window->isOpen())
  {
    sf::Joystick::update();
    keyboardEvents(0);
    for( int i = 0; i < 2; i++)
    {
      if (sf::Joystick::isConnected(i))
        controllerEvents(i);
    }
    if(!window->isOpen() )
        return NULL;

    if(paused == false){
    window->clear();

    sf::Time elapsed1 = clock.getElapsedTime();
    clock.restart();
    float timeElapsed = elapsed1.asSeconds();
    timer += timeElapsed;

    // Update the positions of the elements:
    for( int i = 0; i < world.getNumberCharacters(); i++)
      if( world.getCharacter(i)->update(timeElapsed) == - 1 )
      {
        world.removeCharacter(i);
        i--;
      }

    for( int i = 0; i < world.getNumberPixels(); i++)
      if( world.getPixel(i)->update(timeElapsed, i) == -1 )
      {
        world.removePixel(i);
        i--;
      }


    for( int i = 0; i < world.getNumberShots(); i++)
      if( world.getShot(i)->update(timeElapsed) == -1 )
      {
        world.removeShot(i);
        i--;
      }

    for( int i = 0; i < world.getNumberTexts(); i++)
      if( world.getText(i)->update(timeElapsed) == -1 )
      {
        world.removeText(i);
        i--;
      }

    background->update(timeElapsed);
    timerDisplay.update(timer);

    draw();
    window->display();

    // End Game if a player died:
    if( world.getCharacter(0)->getHealth() == 0 )
        return new EndMenu(window, this, 2, round, player1NumberWins,player2NumberWins);
    else if ( world.getCharacter(1)->getHealth() == 0)
        return new EndMenu(window, this, 1, round, player1NumberWins,player2NumberWins);

    if( timer >= 60 )
    {
      if( score1 > score2 )
        return new EndMenu(window, this, 1, round, player1NumberWins,player2NumberWins);
      else if ( score1 < score2)
        return new EndMenu(window, this, 2, round, player1NumberWins,player2NumberWins);
      else
        return new EndMenu(window, this, -1, round, player1NumberWins,player2NumberWins);
    }

    frameCounter++;
    timeElapsedFPS += timeElapsed;
    if( timeElapsedFPS > 1 )
    {
        fps = frameCounter / timeElapsedFPS;
        frameCounter = 0;
        timeElapsedFPS = 0;
        updateFPS(fps);
    }
  }
    else
    {
      Pause* pauseScreen = new Pause(window, this);
      return pauseScreen;
    }
  }
}

Game::~Game()
{
    delete(background);
}

void Game::draw()
{
    background->draw();



    for( int i = 0; i < ledLeft.size(); i++)
        window->draw(ledLeft.at(i));

    for( int i = 0; i < ledRight.size(); i++)
        window->draw(ledRight.at(i));

    window->draw(scoreBase1);
    window->draw(scoreBase2);
    world.drawGround();

    // displayFPS();
    displayHealth();
    displayScores();

    // Draw the elements:
    for( int i = 0; i < world.getNumberCharacters(); i++)
      world.getCharacter(i)->draw(window);

    for( int i = 0; i < world.getNumberPixels(); i++)
      world.getPixel(i)->draw(window);

    for( int i = 0; i < world.getNumberShots(); i++)
      world.getShot(i)->draw(window);

    for( int i = 0; i < world.getNumberTexts(); i++)
      world.getText(i)->draw(window);

    background->drawForeground();

    timerDisplay.draw();
}

void Game::setUpLeds()
{
    int i = 0;
    sf::Vector2f nextPos(LED_LEFT_X * Setting::scaling.x, LED_LEFT_Y * Setting::scaling.y);
    for( i = 0; i < player1NumberWins; i++)
    {
        ledLeft.push_back(sf::Sprite());
        ledLeft.at(i).setTexture(*ImageStorage::getTexture(std::string("ledOn")));
        ledLeft.at(i).setPosition(nextPos);
        nextPos.x += ledLeft.at(i).getGlobalBounds().width + 5 * Setting::scaling.x;
    }

    for( ; i < 3; i++)
    {
        ledLeft.push_back(sf::Sprite());
        ledLeft.at(i).setTexture(*ImageStorage::getTexture(std::string("ledOff")));
        ledLeft.at(i).setPosition(nextPos);
        nextPos.x += ledLeft.at(i).getGlobalBounds().width + 5 * Setting::scaling.x;
    }

    i = 0;
    nextPos.x  = LED_RIGHT_X * Setting::scaling.x;
    nextPos.y = LED_RIGHT_Y * Setting::scaling.y;
    for( i = 0; i < player2NumberWins; i++)
    {
        ledRight.push_back(sf::Sprite());
        ledRight.at(i).setTexture(*ImageStorage::getTexture(std::string("ledOn")));
        ledRight.at(i).setPosition(nextPos);
        nextPos.x += ledRight.at(i).getGlobalBounds().width + 5 * Setting::scaling.x;
    }

    for( ; i < 3; i++)
    {
        ledRight.push_back(sf::Sprite());
        ledRight.at(i).setTexture(*ImageStorage::getTexture(std::string("ledOff")));
        ledRight.at(i).setPosition(nextPos);
        nextPos.x += ledRight.at(i).getGlobalBounds().width + 5 * Setting::scaling.x;
    }
}

void Game::updateFPS(int fps)
{
    std::ostringstream convert;
    convert << fps;
    textFPS.setString(convert.str());
}

void Game::displayHealth()
{
  float health1Ratio = world.getCharacter(0)->getHealth() / 100.0f;
  health1.setTextureRect(sf::IntRect(0, 0, ImageStorage::getTexture(std::string("life"))->getSize().x * health1Ratio, ImageStorage::getTexture(std::string("life"))->getSize().y));
  window->draw(health1);

  float health2Ratio =  world.getCharacter(1)->getHealth() / 100.0f;
  health2.setTextureRect(sf::IntRect( (int)(ImageStorage::getTexture(std::string("life"))->getSize().x - ImageStorage::getTexture(std::string("life"))->getSize().x * health2Ratio), 0, ImageStorage::getTexture(std::string("life"))->getSize().x * health2Ratio, ImageStorage::getTexture(std::string("life"))->getSize().y));
  health2.setPosition( sf::Vector2f((HEALTH2_X_POS + ImageStorage::getTexture(std::string("life"))->getSize().x - ImageStorage::getTexture(std::string("life"))->getSize().x * health2Ratio) * Setting::scaling.x, HEALTH2_Y_POS));
  window->draw(health2);
}

void Game::displayScores()
{
  int half = ((world.getNoOfColumns()-1)/2);
  int* heights = world.getHeights();
  score1 = 0;
  score2 = 0;

  int i;
  for( i = 0; i < half; i++ )
    score2 += heights[i];

  for(i=half; i < world.getNoOfColumns(); i++)
    score1 += heights[i];

  score1 = score1*SCORE_CONSTANT;
  score2 = score2*SCORE_CONSTANT;

  std::ostringstream convert1;
  std::ostringstream convert2;
  convert1 << "Score: " << score1;
  textScore1.setString(convert1.str());
  convert2 <<"Score: " <<  score2;
  textScore2.setString(convert2.str());
  window->draw(textScore1);
  window->draw(textScore2);
}

void Game::displayFPS()
{
    window->draw(textFPS);
}

/*void Game::setUpController()
{

}*/

void Game::controllerEvents(int i)
{
  if( sf::Joystick::isButtonPressed(i, 0) && canSwitch0[i])
  {
    world.getCharacter(i)->selectWeapon(NORMAL);
    canSwitch0[i] = false;
  }
  else if( sf::Joystick::isButtonPressed(i, 1) && canSwitch1[i])
  {
    world.getCharacter(i)->selectWeapon(RICOCHET);
    canSwitch1[i] = false;
  }
  else if( sf::Joystick::isButtonPressed(i, 2) && canSwitch2[i])
  {
    world.getCharacter(i)->selectWeapon(DESTRUCTION);
    canSwitch2[i] = false;
  }
  else if( sf::Joystick::isButtonPressed(i, 9) && canSwitch[i])
  {
    world.getCharacter(i)->nextWeapon();
    canSwitch[i] = false;
  }

  if( !sf::Joystick::isButtonPressed(i, 0) )
    canSwitch0[i] = true;
  if (!sf::Joystick::isButtonPressed(i, 1) )
    canSwitch1[i] = true;
  if (!sf::Joystick::isButtonPressed(i, 2) )
    canSwitch2[i] = true;
  if( !sf::Joystick::isButtonPressed(i, 9))
    canSwitch[i] = true;


  if( sf::Joystick::isButtonPressed(i, 4) && readyToJump[i] )
  {
    world.getCharacter(i)->jump();
    readyToJump[i] = false;
  }
  else if ( !sf::Joystick::isButtonPressed(i, 4) )
    readyToJump[i] = true;

  if( sf::Joystick::getAxisPosition(i, sf::Joystick::X) > 40 )
    world.getCharacter(i)->move(1);
  else if( sf::Joystick::getAxisPosition(i, sf::Joystick::X) < -40 )
    world.getCharacter(i)->move(-1);
  else
  {
    world.getCharacter(i)->cancelMove(-1);
    world.getCharacter(i)->cancelMove(1);
  }

  if( sf::Joystick::getAxisPosition(i, sf::Joystick::U) * sf::Joystick::getAxisPosition(i, sf::Joystick::U)
      + sf::Joystick::getAxisPosition(i, sf::Joystick::R) * sf::Joystick::getAxisPosition(i, sf::Joystick::R) > 5000 )
    world.getCharacter(i)->setFiringTarget(sf::Joystick::getAxisPosition(i, sf::Joystick::R), sf::Joystick::getAxisPosition(i, sf::Joystick::U));

  if( !weaponCharging[i] && abs(sf::Joystick::getAxisPosition(i, sf::Joystick::Z)) > 40  )
  {
    weaponCharging[i] = true;
    startTime[i] = clock2.getElapsedTime().asSeconds();
  }
  else if( weaponCharging[i] && abs(sf::Joystick::getAxisPosition(i, sf::Joystick::Z)) <= 40)
  {
    weaponCharging[i] = false;
    time = clock2.getElapsedTime();
    timeInSeconds = time.asSeconds();
    float chargeTime = timeInSeconds - startTime[i];
    world.getCharacter(i)->fire(timeInSeconds, chargeTime);
  }
}

void Game::keyboardEvents(int i)
{
    sf::Event event;
    while (window->pollEvent(event))
    {
      switch (event.type)
      {
        case sf::Event::Closed:
          window->close();
          break;

        case sf::Event::KeyPressed:
          if( event.key.code == sf::Keyboard::Escape)
            window->close();
          break;

         case sf::Event::JoystickButtonPressed:
                if( event.joystickButton.button == 7)
                    pause();
                break;

        default:
          break;
      }
    }
}

void Game::startAnimation()
{
    sf::Sprite* start = new sf::Sprite();
    sf::Texture texture;
    sf::Texture texture1;
    if( round == 1 )
    {
      texture.loadFromFile("text/round_1.png");
      texture1.loadFromFile("text/round_1_fight.png");
    }
    else if( round == 2 )
    {
      texture.loadFromFile("text/round_2.png");
      texture1.loadFromFile("text/round_2_fight.png");
    }
    else if( round == 3 )
    {
      texture.loadFromFile("text/round_3.png");
      texture1.loadFromFile("text/round_3_fight.png");
    }
    start->setTexture(texture);
    start->setOrigin(start->getGlobalBounds().width / 2, start->getGlobalBounds().height / 2);
    start->setPosition(window->getRealSize().x / 2, window->getRealSize().y / 2);
    int index = 0;
    clock2.restart();
    while( window->isOpen() )
    {
        start->setScale( 1 + clock2.getElapsedTime().asSeconds(), 1 + clock2.getElapsedTime().asSeconds());
        if( clock2.getElapsedTime().asSeconds() > 1 )
        {
            clock2.restart();
            index++;
            if( index > 1 )
                return;
            delete start;
            start = new sf::Sprite();
            start->setTexture(texture1);
            start->setOrigin(start->getGlobalBounds().width / 2, start->getGlobalBounds().height / 2);
            start->setPosition(window->getRealSize().x / 2, window->getRealSize().y / 2);
        }
        window->clear();
        draw();
        window->draw(*start);
        window->display();
    }
}

void Game::pause(){
    if(paused == true)
        paused = false;
    else{
        paused = true;
        clock2.restart();
    }
}

