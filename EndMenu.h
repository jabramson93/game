#ifndef ENDMENU_H
#define ENDMENU_H

#include <SFML/Graphics.hpp>
#include "Playable.h"
#include "PlayWindow.h"
#include "Game.h"

class EndMenu : public Playable
{
    public:
        EndMenu(PlayWindow* windowTemp, Game* previous, int side, int round, int player1NumberWinsTemp, int player2NumberWinsTemp);
        Playable* play();

    protected:
        bool tournamentFinished;
        int player1NumberWins, player2NumberWins;
        int nextRound;
        sf::Font font;
        sf::Texture textureText;
        Game* previous;
        sf::Sprite text;
        PlayWindow* window;
};

#endif
