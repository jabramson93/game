#ifndef ANIMATED_TEXT_H
#define ANIMATED_TEXT_H

#include <SFML/Graphics.hpp>
#include "PlayWindow.h"

class AnimatedText
{
    public:
        AnimatedText(PlayWindow* windowTemp, char* textChar, sf::Vector2f pos);
        int update(float time);
        void draw(PlayWindow* window);

    protected:
        PlayWindow* window;
        float acc;
        sf::Text text;
        sf::Font font;
};

#endif
