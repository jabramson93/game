#include <SFML/Graphics.hpp>
#include <iostream>
#include "Setting.h"
#include "TimerDisplay.h"

TimerDisplay::TimerDisplay(PlayWindow* windowTemp) : size{51, 27, 51, 51, 51, 51, 51, 51, 51, 51}
{
    window = windowTemp;

    texture.loadFromFile("number_sheet.png");
    sprites = (sf::Sprite*)malloc(sizeof(sf::Sprite)*2);
    sprite0.setTexture(texture);
    sprite0.setScale(Setting::scaling.x, Setting::scaling.y);
    xSize = texture.getSize().x / 10;
    ySize = texture.getSize().y;


    sprite1.setTexture(texture);
    sprite1.setScale(Setting::scaling.x, Setting::scaling.y);
    timeOld = -1;
    update(0);
    center();
}

void TimerDisplay::center()
{
    int width = 10 * Setting::scaling.x + sprite0.getGlobalBounds().width + sprite1.getGlobalBounds().width;
    sprite0.setPosition(sf::Vector2f(window->getRealSize().x / 2 - width / 2, 10 ));
    sprite1.setPosition(sf::Vector2f(window->getRealSize().x / 2 - width / 2 + sprite0.getGlobalBounds().width + 10 * Setting::scaling.x, 10 * Setting::scaling.y ));
}

void TimerDisplay::update(float time)
{
    if( time == timeOld )
        return;
    time = 60 - time;
    sprite0.setTextureRect(sf::IntRect(getStart(getDigit(time, 10)), 0, size[getDigit(time, 10)], ySize));
    sprite1.setTextureRect(sf::IntRect(getStart(getDigit(((int)time)%10, 1)), 0, size[getDigit(((int)time)%10, 1)], ySize));
    center();
}

int TimerDisplay::getStart(int index)
{
    int start = 0;
    for( int i = 0; i < index; i++)
        start += size[i];
    start += 3 * index;
    return start;
}

int TimerDisplay::getDigit(int number, int decimal)
{
    int counter = 0;
    while( number >= decimal )
    {
        counter++;
        number -= decimal;
    }
    return counter;
}

void TimerDisplay::draw()
{
    window->draw(sprite0);
    window->draw(sprite1);
}

