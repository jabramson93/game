#include <cmath>
#include <iostream>
#include <string>

#include "Character.h"
#include "ImageStorage.h"
#include "Pixel.h"
#include "Shot.h"
#include "Setting.h"
#include "PlayWindow.h"
#include "AnimatedText.h"
#include "MusicMixer.h"

#define PI (3.141592653589793)

Character::Character(int X, int Y, int velX, int velY, sf::Texture* texture, World& w, PlayWindow& window, sf::Texture* targetT, int directionTemp, int sideTemp) : Object( X, Y, velX, velY, texture, w, window)
{
  gripping = false;
  nbFrames = 5;
  index = 0;
  totalTime = 0;
  armMvtOffset = 0;
  armMvtDirection = 1;
  direction = 1;
  firingAngleSpeed = 0;

  damageSprite.setTexture(*ImageStorage::getTexture(std::string("playerDamage")));
  targetSprite.setTexture(*targetT);
  targetSprite.setScale(sf::Vector2f(Setting::scaling.x, Setting::scaling.y));

  takingDamage = false;

  targetSprite2.setTexture(*ImageStorage::getTexture(std::string("arm2")));
  targetSprite2.setScale(sf::Vector2f(Setting::scaling.x, Setting::scaling.y));

  if( directionTemp == 1)
    firingAngle = 0;
  else
    firingAngle = 180;
  checkFiringAngle();
  targetSprite.setOrigin( ARM_ORIGIN_X, ARM_ORIGIN_Y);
  targetSprite2.setOrigin( ARM_ORIGIN_X, ARM_ORIGIN_Y);
  bouncing = false;
  health = 100;
  doubleJump = false;
  side = sideTemp;
  firingTime = 0;

  velocity.x = 0;
  velocity.y = 0;
  moveDirection = 0;

  gunType = NORMAL;
  dimensionSprite.x = ImageStorage::getTexture(std::string("player"))->getSize().x / nbFrames;
  dimensionSprite.y = ImageStorage::getTexture(std::string("player"))->getSize().y;

  dimension.x = Setting::scaling.x * ImageStorage::getTexture(std::string("player"))->getSize().x / nbFrames;
  dimension.y = Setting::scaling.y * ImageStorage::getTexture(std::string("player"))->getSize().y;
  //dimensionSprite.y = sprite.getGlobalBounds().height;
  updateWeapongImages();
  setRechargeTime();
}

void Character::updateWeapongImages()
{
    switch( gunType )
    {
        case NORMAL:
            targetSprite.setTexture(*ImageStorage::getTexture(std::string("weapon_1")));
            break;
        case RICOCHET:
            targetSprite.setTexture(*ImageStorage::getTexture(std::string("weapon_2")));
            break;
        case DESTRUCTION:
            targetSprite.setTexture(*ImageStorage::getTexture(std::string("weapon_3")));
            break;
    }
}

void Character::checkObjectIsInBound(float timeElapsed)
{
  if( position.y + dimension.y > window.getSize().y )
  {
    if( bouncing )
      velocity.y = - 0.5 * velocity.y;
    else
      velocity.y = 0;
    position.y = window.getSize().y - dimension.y;
    reactWallCollision();
  }
  else if ( position.y < 0 )
  {
    if( bouncing )
      velocity.y = - 0.5 * velocity.y;
    else
      velocity.y = 0;
    position.y = 0;
    reactWallCollision();
  }

  bool stillGripping = false;
  if( position.x + dimension.x > window.getSize().x )
  {
    position.x = window.getSize().x - dimension.x;
    if( bouncing )
      velocity.x = - 0.5 * velocity.x;
    else if( velocity.y > 0 && velocity.x > 0 )
    {
      if ( !gripping )
        index = 0;
      stillGripping = true;
      velocity.y -= WALL_GRIPPING_VELOCITY * timeElapsed;
      if( direction == 1)
        flip();
      sprite.setTexture(*ImageStorage::getTexture(std::string("playerGripping")));
      damageSprite.setTexture(*ImageStorage::getTexture(std::string("playerGrippingDamage")));
      position.x += 20 * Setting::scaling.x;
      nbFrames = 4;
      doubleJump = false;
      if( velocity.y < 0)
        velocity.y = 0;
    }
    reactWallCollision();
  }
  else if ( position.x < 0 )
  {
    position.x = 0;
    if( bouncing )
      velocity.x = - 0.5 * velocity.x;
    else if( velocity.y > 0 && velocity.x < 0)
    {
      if ( !gripping )
        index = 0;
    if( direction == -1)
        flip();
      stillGripping = true;
      sprite.setTexture(*ImageStorage::getTexture(std::string("playerGripping")));
      damageSprite.setTexture(*ImageStorage::getTexture(std::string("playerGrippingDamage")));
      position.x -= 20 * Setting::scaling.x;
      nbFrames = 4;
      velocity.y -= WALL_GRIPPING_VELOCITY * timeElapsed;
      doubleJump = false;
      if( velocity.y < 0)
        velocity.y = 0;
    }
    reactWallCollision();
  }

  if( !stillGripping && gripping )
  {
    sprite.setTexture(*ImageStorage::getTexture(std::string("playerRunning")));
    damageSprite.setTexture(*ImageStorage::getTexture(std::string("playerRunningDamage")));
    index = 0;
    nbFrames = 8;
    gripping = false;
  }
  else
  {
      gripping = stillGripping;
  }

  if( side == 1)
  {
    if( position.x + dimension.x > window.getSize().x / 2)
      position.x = window.getSize().x / 2 - dimension.x;
  }
  else if ( side == 2)
  {
    if( position.x  < window.getSize().x / 2)
      position.x = window.getSize().x / 2;
  }
}

void Character::updatePositionAfterGroundCollision(sf::Rect<float> objPos, float elapsedTime)
{
  sf::Vector2f oldPosition = lastPosition;
  if( velocity.x == 0 || !( oldPosition.x + dimension.x < objPos.left || oldPosition.x > objPos.left + objPos.width ) )
  {
    velocity.y = 0;
    position.y = lastPosition.y;
    if( collide(elapsedTime, false) )
      position = lastPosition;
  }
  else if( velocity.y == 0 || !( oldPosition.y + dimension.y < objPos.top || oldPosition.y > objPos.top + objPos.height ) )
  {
    if( oldPosition.y + dimension.y - objPos.top < MAXIMUM_NUMBER_PIXEL_CLIMBED * 10 * Setting::scaling.y )
    {
      position.y =  objPos.top - dimension.y - 1;
      return;
    }
    velocity.x = 0;
    position.x = lastPosition.x;
    if( collide(elapsedTime, false) )
      position = lastPosition;
  }
  else
  {
    position = lastPosition;
    velocity.x = 0;
    velocity.y = 0;
  }
  sprite.setPosition(position);
  damageSprite.setPosition(position);
}

sf::FloatRect Character::getBoundingBox()
{
    sf::FloatRect boundingBox = sprite.getGlobalBounds();
    boundingBox.left = position.x;
    boundingBox.top = position.y;
    boundingBox.width = dimension.x;
    boundingBox.height = dimension.y;
    return boundingBox;
}

bool Character::collideGround(float elapsedTime, bool react)
{
  bool response = world.checkGroundCollision(getBoundingBox(), 0);
  if( response && react )
  {
    updatePositionAfterGroundCollision(world.getLastGroundCollision(), elapsedTime);
  }
  return response;
}


sf::Vector2i Character::setRotation( float x, float y)
{
  float newAngle;
  sf::Vector2i newMousePosition;
  if( direction == 1 )
  {
    newAngle = atan2( ( y - position.y - RIGHT_ARM_OFFSET_Y * Setting::scaling.y) , x - position.x - RIGHT_ARM_OFFSET_X * Setting::scaling.x);
    newMousePosition.x = position.x + RIGHT_ARM_OFFSET_X * Setting::scaling.x + cos(newAngle) * MOUSE_DISTANCE;
    newMousePosition.y = position.y + RIGHT_ARM_OFFSET_Y * Setting::scaling.y + sin(newAngle) * MOUSE_DISTANCE;
  }
  else
  {
    newAngle = atan2( ( y - position.y - LEFT_ARM_OFFSET_Y * Setting::scaling.y) , x - position.x - LEFT_ARM_OFFSET_X * Setting::scaling.x);
    newMousePosition.x = position.x + LEFT_ARM_OFFSET_X * Setting::scaling.x + cos(newAngle) * MOUSE_DISTANCE;
    newMousePosition.y = position.y + LEFT_ARM_OFFSET_Y * Setting::scaling.y + sin(newAngle) * MOUSE_DISTANCE;
  }
  firingAngle = newAngle * 180 / PI;
  checkFiringAngle();
  return newMousePosition;
}

void Character::setFiringTarget( float x, float y)
{
  float newAngle = atan2( x , y);
  firingAngle = newAngle * 180 / PI;
  checkFiringAngle();
}

void Character::jump()
{
  if( isOnGround() )
  {
    addVelocity(sf::Vector2f(0, SPEED_FIRST_JUMP ));
    doubleJump = false;
  }
  else if( doubleJump == false)
  {
    setVelocity(sf::Vector2f(0, SPEED_SECOND_JUMP ));
    doubleJump = true;
  }

}

void Character::draw(PlayWindow* window)
{
  if( direction == 1)
  {
    sprite.setTextureRect(sf::IntRect(index * dimensionSprite.x, 0, dimensionSprite.x, dimensionSprite.y));
    damageSprite.setTextureRect(sf::IntRect(index * dimensionSprite.x, 0, dimensionSprite.x, dimensionSprite.y));

    targetSprite.setRotation(firingAngle);
    targetSprite.setPosition(sf::Vector2f(position.x + RIGHT_ARM_OFFSET_X * Setting::scaling.x, position.y + armMvtOffset + RIGHT_ARM_OFFSET_Y * Setting::scaling.y));
    targetSprite2.setRotation(firingAngle);
    targetSprite2.setPosition(sf::Vector2f(position.x + RIGHT_ARM_OFFSET_X * Setting::scaling.x, position.y + armMvtOffset + RIGHT_ARM_OFFSET_Y * Setting::scaling.y));
  }
  else
  {
    sprite.setOrigin(sprite.getGlobalBounds().width / Setting::scaling.x, 0);
    sprite.setTextureRect(sf::IntRect(index * dimensionSprite.x, 0, dimensionSprite.x, dimensionSprite.y));

    damageSprite.setOrigin(sprite.getGlobalBounds().width / Setting::scaling.x, 0);
    damageSprite.setTextureRect(sf::IntRect(index * dimensionSprite.x, 0, dimensionSprite.x, dimensionSprite.y));

    targetSprite.setRotation(180+firingAngle);
    targetSprite.setPosition(sf::Vector2f( position.x + LEFT_ARM_OFFSET_X * Setting::scaling.x, position.y + armMvtOffset + LEFT_ARM_OFFSET_Y * Setting::scaling.y));
    targetSprite2.setRotation(180+firingAngle);
    targetSprite2.setPosition(sf::Vector2f( position.x + LEFT_ARM_OFFSET_X * Setting::scaling.x, position.y + armMvtOffset + LEFT_ARM_OFFSET_Y * Setting::scaling.y));
  }
  if( !gripping )
    window->draw(targetSprite2, true);
  Object::draw(window);
  window->draw(targetSprite, true);
  if( takingDamage )
    window->draw(damageSprite, true);
  sprite.setOrigin(0,0);
  damageSprite.setOrigin(0,0);
}


int Character::update(float timeElapsed )
{
  if( takingDamage )
  {
      damageTime += timeElapsed;
      if( damageTime > 0.1 )
        takingDamage = false;
  }
  velocity.x = CHARACTER_SPEED * Setting::scaling.x * moveDirection;

  totalTime += timeElapsed;
  if(  totalTime > TIME_BY_FRAME && moveDirection == 0 )
  {
    index++;
      if( index >= nbFrames)
        index = 0;
      totalTime = 0;
  }
  else if ( totalTime > TIME_BY_FRAME_RUNNING && moveDirection != direction )
  {
      index--;
      if( index < 0)
        index = nbFrames-1;
      totalTime = 0;
  }
  else if ( totalTime > TIME_BY_FRAME_RUNNING && moveDirection == direction )
  {
      index++;
      if( index >= nbFrames)
        index = 0;
      totalTime = 0;
  }
  armMvtOffset += timeElapsed * ARM_OFFSET_SPEED * armMvtDirection;
  if( armMvtOffset < 0 || armMvtOffset > MAXIMUM_ARM_MOVMENT_OFFSET)
    armMvtDirection = -armMvtDirection;
  if( firingAngle != 0 )
  {
    firingAngle += firingAngleSpeed * timeElapsed;
    checkFiringAngle();
  }
  if( !isOnGround() )
    velocity.y += gravity * timeElapsed;
  lastPosition = position;
  position += velocity * timeElapsed;
  sprite.setPosition(position);
  collide(timeElapsed, true);
  checkObjectIsInBound(timeElapsed);
  sprite.setPosition(position);
  damageSprite.setPosition(position);
  return 1;
}

void Character::takeDamage(int damage)
{
  MusicMixer::playDamageMusic();
  health = health - damage;
  if( health < 0 )
    health = 0;
  takingDamage = true;
  damageTime = 0;
}

void Character::moveFiringTarget( int direction )
{
  firingAngleSpeed = direction * FIRING_ANGLE_SPEED;
}

void Character::cancelMoveFiringTarget( int direction )
{
  if( std::abs(firingAngleSpeed - ( direction * FIRING_ANGLE_SPEED )) < 0.005 )
    firingAngleSpeed = 0;
}

void Character::checkFiringAngle()
{
  float tempFiringAngle = ((int)firingAngle) % 360;
  if( tempFiringAngle < 0 )
    tempFiringAngle = 360 + tempFiringAngle;
  if( direction == 1 && tempFiringAngle > 90 && tempFiringAngle < 270)
    flip();
  else if( direction == -1 && ( tempFiringAngle < 90 || tempFiringAngle > 270))
    flip();
}

int Character::getHealth(){
  return health;
}

void Character::flip()
{
  if( gripping )
    return;
  direction = - direction;
  sprite.setOrigin(sprite.getGlobalBounds().width / 2, sprite.getGlobalBounds().height / 2);
  sprite.scale(-1, 1);
  sprite.setOrigin(0, 0);

  damageSprite.setOrigin(sprite.getGlobalBounds().width / 2, sprite.getGlobalBounds().height / 2);
  damageSprite.scale(-1, 1);
  damageSprite.setOrigin(0, 0);

  targetSprite.setOrigin(targetSprite.getGlobalBounds().width/2, targetSprite.getGlobalBounds().height/2);
  targetSprite.scale(-1, 1);
  targetSprite.setOrigin( ARM_ORIGIN_X, ARM_ORIGIN_Y);

  targetSprite2.setOrigin(targetSprite.getGlobalBounds().width/2, targetSprite.getGlobalBounds().height/2);
  targetSprite2.scale(-1, 1);
  targetSprite2.setOrigin( ARM_ORIGIN_X, ARM_ORIGIN_Y);
}

void Character::move(int directionTemp)
{
  if( moveDirection != directionTemp )
  {
    sprite.setTexture(*ImageStorage::getTexture(std::string("playerRunning")));
    nbFrames = 8;
    dimensionSprite.x = ImageStorage::getTexture(std::string("playerRunning"))->getSize().x / nbFrames;
    dimensionSprite.y = ImageStorage::getTexture(std::string("playerRunning"))->getSize().y;
    index = 0;
  }
  moveDirection = directionTemp;

}

void Character::cancelMove( int directionTemp )
{
  if( directionTemp == moveDirection )
  {
    index = 0;
    moveDirection = 0;
    sprite.setTexture(*ImageStorage::getTexture(std::string("player")));
    damageSprite.setTexture(*ImageStorage::getTexture(std::string("playerDamage")));
    nbFrames = 5;
    dimensionSprite.x = ImageStorage::getTexture(std::string("player"))->getSize().x / nbFrames;
    dimensionSprite.y = ImageStorage::getTexture(std::string("player"))->getSize().y;
  }
}

float Character::getXDisplayPosition()
{
  if( direction == 1 )
    return position.x;
  else
    return position.x + dimensionSprite.x;
}

void Character::fire(float time, float chargeTime)
{
  if( time - firingTime > rechargeTime )
  {
    firingTime = time;
    int weapongLength = 110 * Setting::scaling.x;
    float radianAngle = firingAngle * PI / 180;
    float shotSpeed = SHOT_SPEED;
    float velX = cos(radianAngle) * shotSpeed;
    float velY = sin(radianAngle) * shotSpeed;
    float startX, startY;
    if( direction == 1 )
    {
      startX = position.x + RIGHT_ARM_OFFSET_X + weapongLength * cos(radianAngle);
      startY = position.y + RIGHT_ARM_OFFSET_Y + weapongLength * sin(radianAngle);
    }
    else
    {
      startX = position.x + LEFT_ARM_OFFSET_X + weapongLength * cos(radianAngle);
      startY = position.y + LEFT_ARM_OFFSET_Y + weapongLength * sin(radianAngle);
    }
    MusicMixer::playShotMusic();
    if( gunType == NORMAL )
      world.add(new Shot(startX, startY, velX, velY, ImageStorage::getTexture(std::string("shotBlue")), world, window, radianAngle, this, chargeTime, gunType, side));
    else if( gunType == RICOCHET )
      world.add(new Shot(startX, startY, velX, velY, ImageStorage::getTexture(std::string("shotGreen")), world, window, radianAngle, this, chargeTime, gunType, side));
    else if( gunType == DESTRUCTION )
      world.add(new Shot(startX, startY, velX, velY, ImageStorage::getTexture(std::string("shotRed")), world, window, radianAngle, this, chargeTime, gunType, side));
  }
}

bool Character::collide(float timeElapsed, bool react)
{
  bool result = false;
  if( react )
    result = result || collideShots(timeElapsed, react);
  result = result || collideGround(timeElapsed, react);
  return result;
}

bool Character::collideShots(float timeElapsed, bool react)
{
  // Test collisions with characters:
  for( int i = 0; i < world.getNumberShots() ; i++ )
    if( world.getShot(i)->parent != (Object*)this && collision((Object*)world.getShot(i) ) )
    {
      if( react )
        world.getShot(i)->reactCollisionCharacter(this);
      return true;
    }
  return false;
}

void Character::setRechargeTime()
{
    if( gunType == NORMAL )
        rechargeTime = RECHARGE_TIME;
    else if( gunType == RICOCHET)
        rechargeTime = RECHARGE_TIME * 1.5;
    else if( gunType == DESTRUCTION)
        rechargeTime = RECHARGE_TIME * 2;
}

bool Character::collidePixels(float elapsedTime, bool react)
{
  for( int i = 0; i < world.getNumberPixels() ; i++ )
  {
    if( collision((Object*)world.getPixel(i) ))
    {
      if( react )
      {
        sf::Vector2f addedVel(velocity);
        addedVel.x /= 2;
        addedVel.y /= 2;
        world.getPixel(i)->addVelocity(addedVel);
        world.getPixel(i)->reactCollisionCharacter(this);
        updatePositionAfterCollision(world.getPixel(i), elapsedTime);
      }
      return true;
    }
  }
  return false;
}

void Character::nextWeapon()
{
    gunType = (ShotType)(gunType + 1);
    if( gunType > 2)
        gunType = (ShotType)0;
    MusicMixer::playChangeWeaponMusic();
    world.add(new AnimatedText(&window, getText(), position));
    updateWeapongImages();
    setRechargeTime();
}

char* Character::getText()
{
    if( gunType == NORMAL )
        return "Normal";
    else if( gunType == RICOCHET )
        return "Ricochet";
    else if( gunType == DESTRUCTION )
        return "Destruction";
}

void Character::selectWeapon(ShotType newType)
{
    gunType = newType;
    MusicMixer::playChangeWeaponMusic();
    world.add(new AnimatedText(&window, getText(), position));
    updateWeapongImages();
    setRechargeTime();
}
