#ifndef GROUND_H
#define GROUND_H

#include <SFML/Graphics.hpp>
#include "PlayWindow.h"

#include <vector>


class World;

class Ground
{
  public:
    Ground(int numberOfSquareTemp, int numberOfverticalSquares, sf::Vector2f sizeOfSquare, PlayWindow* w, World* wo);
    bool add(sf::Rect<float> pos, int indexPixel);
    bool removeLayer();
    void draw();
    bool collision(sf::Rect<float> pos, int error);
    sf::Rect<float> getLastCollision();
    void emptySpace(sf::Rect<float> pos, int index);
    void emergencyAdd(sf::Rect<float> pos, int indexPixel);
    void addPixel(int i, int indexPixel);
    int getColumnSize();
    int* getHeights();
    void destroy(sf::Rect<float> pos);
    void destroy(int column, int start, int end);

  protected:
    World* world;
  	int* heights;
  	int numberOfSquare;
    sf::Vector2f sizeSquare;
  	std::vector<std::vector<sf::Sprite> > groudSprites;
    std::vector<std::vector<sf::Sprite> > groudSpritesShadow;
    std::vector<std::vector<sf::Sprite> > groudSpritesColor;
  	int radiusX, radiusY;
  	PlayWindow* window;
    int lastI, lengthI;
    int numberOfverticalSquares;
};

#endif
