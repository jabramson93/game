#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>
#include <vector>
#include <SFML/Audio.hpp>

#include "Character.h"
#include "Shot.h"
#include "Pixel.h"
#include "Background.h"
#include "Playable.h"
#include "PlayWindow.h"
#include "TimerDisplay.h"


class Game : public Playable
{
  public:
    Game(PlayWindow* windowTemp, Playable* previous, int roundTemp = 1, int player1NumberWins = 0, int player2NumberWins = 0);
    ~Game();
    Playable* play();
    void setText(sf::Text& text, float positonx, float positionY);
    void displayFPS();
    void updateFPS(int fps);
    void displayHealth();
    void displayScores();
    void keyboardEvents(int i);
    void controllerEvents(int i);
    void pause();
    void draw();
    void startAnimation();
    void setUpLeds();

  protected:
    int round;
    PlayWindow* window;
    sf::Sprite health1, health2, scoreBase1, scoreBase2;
    std::vector<sf::Sprite> ledLeft;
    std::vector<sf::Sprite> ledRight;

    World world;
    Background* background;
    sf::Font fontFPS;
    sf::Text textFPS;
    sf::Text textScore1;
    sf::Text textScore2;
    sf::Text textTimer;
    int score1, score2;

    // Variables related to controller:
    bool readyToJump[2];
    bool weaponCharging[2];
    bool canSwitch[2], canSwitch0[2], canSwitch1[2], canSwitch2[2];

    // Variables related to keyboard:
    bool jumpKey = false;
    bool jumpKey2 = false;
    bool ignore = false;
    sf::Vector2i newMousePosition;

    // Sahred between controllers and keyboard:
    float timeInSeconds, chargeTime;
    float startTime[2];
    sf::Clock clock2;
    float timer;
    sf::Time time;

    TimerDisplay timerDisplay;
    int player1NumberWins, player2NumberWins;

    bool paused;

};

#endif
