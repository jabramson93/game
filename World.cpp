#include "World.h"
#include "AnimatedText.h"
#include <iostream>


World::World()
{

}

World::~World()
{
  while( characters.size() > 0 )
    removeCharacter(0);

  while( pixels.size() > 0 )
    removePixel(0);

  while( shots.size() > 0 )
    removeShot(0);

  delete(ground);
}

void World::drawGround()
{
  ground->draw();
}

void World::addGround(Ground* groundTemp)
{
  ground = groundTemp;
}

void World::add(Character* c)
{
  characters.push_back(c);
}

void World::add(AnimatedText* t)
{
  texts.push_back(t);
}

void World::add(Pixel* o)
{
  pixels.push_back(o);
}

void World::add(Shot* s)
{
  shots.push_back(s);
}

Shot* World::getShot(int i)
{
  return shots[i];
}

Pixel* World::getPixel(int i)
{
  return pixels[i];
}

AnimatedText* World::getText(int i)
{
  return texts[i];
}

Character* World::getCharacter(int i)
{
  return characters[i];
}

void World::removeShot(int i)
{
  delete(shots.at(i));
  shots.erase(shots.begin() + i);
}

void World::removeText(int i)
{
  delete(texts.at(i));
  texts.erase(texts.begin() + i);
}

void World::removePixel(int i)
{
  delete(pixels.at(i));
  pixels.erase(pixels.begin() + i);
}

void World::removeCharacter(int i)
{
  delete(characters.at(i));
  characters.erase(characters.begin() + i);
}

int World::getNumberShots()
{
  return shots.size();
}

int World::getNumberTexts()
{
  return texts.size();
}

int World::getNumberCharacters()
{
  return characters.size();
}

int World::getNumberPixels()
{
  return pixels.size();
}

bool World::checkGroundCollision(sf::Rect<float> pos, int error)
{
  return ground->collision(pos, error);
}

bool World::addToGround(sf::Rect<float> pos, int index)
{
  return ground->add(pos, index);
}

int World::getNoOfColumns(){
  return ground->getColumnSize();
}

int* World::getHeights(){
  return ground->getHeights();
}

void World::emergencyAddToGround(sf::Rect<float> pos, int index)
{
  ground->emergencyAdd(pos, index);
}

void World::destroyPixels(sf::Rect<float> pos)
{
  ground->destroy(pos);
}


sf::Rect<float> World::getLastGroundCollision()
{
  return ground->getLastCollision();
}
