#include "PlayWindow.h"

#include "Setting.h"

PlayWindow::PlayWindow(sf::RenderWindow* windowTemp) : view(sf::FloatRect(0, 0, 1366, 768))
{
    window = windowTemp;
    window->setView(view);
    view.setViewport(sf::FloatRect(0, 0, 1 / ( 1366 * Setting::scaling.x ) , 1 / ( 768 * Setting::scaling.y)));
    Setting::scaling.x = 1;
    Setting::scaling.y = 1;
}

sf::Vector2u PlayWindow::getSize()
{
    return sf::Vector2u( 1366 - 40 , 768 - 40);
}

sf::Vector2u PlayWindow::getStart()
{
    return sf::Vector2u( 20, 20 );
}

sf::Vector2u PlayWindow::getRealSize()
{
    return sf::Vector2u( 1366, 768);
}

void PlayWindow::draw(sf::Sprite& drawable, bool game = true)
{
    if( game )
        drawable.move(20, 20);
    window->draw(drawable);
    if( game )
        drawable.move(-20, -20);
}

void PlayWindow::draw(const sf::Drawable& 	drawable)
{
    window->draw(drawable);
}

bool PlayWindow::isOpen()
{
    return window->isOpen();
}

void PlayWindow::close()
{
    window->close();
}

void PlayWindow::clear()
{
    window->clear();
}

void PlayWindow::display()
{
    window->display();
}

sf::RenderWindow* PlayWindow::getWindow()
{
    return window;
}

bool PlayWindow::pollEvent(sf::Event& event)
{
    return window->pollEvent(event);
}
