#include "Credits.h"

#include "StartMenu.h"
#include "Setting.h"
#include "PlayWindow.h"

Credits::Credits(PlayWindow* windowTemp) : Playable(), clock()
{
    window = windowTemp;
    startPosition = window->getSize().y / 2;
    numberText = 12;

    texts = (sf::Text**)malloc(sizeof(sf::Text*) * numberText);

    font.loadFromFile("Minecraftia.ttf");

    for( int i = 0 ; i < numberText; i++)
    {
        texts[i] = new sf::Text();
        texts[i]->setFont(font);
    }

    texts[0]->setString(std::string("Pixel Leak"));
    texts[0]->setCharacterSize(40*Setting::scaling.y);
    texts[0]->setStyle(sf::Text::Bold);

    texts[1]->setString(std::string("The development team"));
    texts[1]->setStyle(sf::Text::Bold);
    texts[1]->setCharacterSize(28*Setting::scaling.y);

    texts[2]->setString(std::string("Lead Programmer"));
    texts[2]->setStyle(sf::Text::Bold);
    texts[2]->setCharacterSize(24*Setting::scaling.y);

    texts[3]->setString(std::string("Jessie Abramson"));
    texts[3]->setCharacterSize(20*Setting::scaling.y);

    texts[4]->setString(std::string("Programmer"));
    texts[4]->setStyle(sf::Text::Bold);
    texts[4]->setCharacterSize(24*Setting::scaling.y);

    texts[5]->setString(std::string("Anthony Glover"));
    texts[5]->setCharacterSize(20*Setting::scaling.y);

    texts[6]->setString(std::string("Designer"));
    texts[6]->setStyle(sf::Text::Bold);
    texts[6]->setCharacterSize(24*Setting::scaling.y);

    texts[7]->setString(std::string("Alexander Cooke"));
    texts[7]->setCharacterSize(20*Setting::scaling.y);

    texts[8]->setString(std::string("Artist"));
    texts[8]->setStyle(sf::Text::Bold);
    texts[8]->setCharacterSize(24*Setting::scaling.y);

    texts[9]->setString(std::string("Alexander Cooke"));
    texts[9]->setCharacterSize(20*Setting::scaling.y);

    texts[10]->setString(std::string("Music"));
    texts[10]->setStyle(sf::Text::Bold);
    texts[10]->setCharacterSize(28*Setting::scaling.y);

    texts[11]->setString(std::string("Minecraft Forest by NubbyCakums"));
    texts[11]->setCharacterSize(20*Setting::scaling.y);

    int spacing[] = {0,1,3,5,7};
    int numberSpacing = 5;
    int nextSpacing = 0;
    int newPosition = startPosition;
    int i;
    for( i = 0 ; i < 2; i++)
    {
        texts[i]->setPosition(sf::Vector2f( window->getRealSize().x / 2 - texts[i]->getGlobalBounds().width / 2, newPosition));
        newPosition += texts[i]->getGlobalBounds().height + 20 * Setting::scaling.y;
        if( nextSpacing < numberSpacing && spacing[nextSpacing] == i )
        {
            nextSpacing++;
            newPosition += 40 * Setting::scaling.y;
        }
    }

    float savedNewPosition = newPosition;
    for( ; i < 6; i++)
    {
        texts[i]->setPosition(sf::Vector2f( window->getRealSize().x / 4 - texts[i]->getGlobalBounds().width / 2, newPosition));
        newPosition += texts[i]->getGlobalBounds().height + 20 * Setting::scaling.y;
        if( nextSpacing < numberSpacing && spacing[nextSpacing] == i )
        {
            nextSpacing++;
            newPosition += 40 * Setting::scaling.y;
        }
    }

    newPosition = savedNewPosition;
    for( ; i < 10; i++)
    {
        texts[i]->setPosition(sf::Vector2f( window->getRealSize().x / 4 * 3 - texts[i]->getGlobalBounds().width / 2, newPosition));
        newPosition += texts[i]->getGlobalBounds().height + 20 * Setting::scaling.y;
        if( nextSpacing < numberSpacing && spacing[nextSpacing] == i )
        {
            nextSpacing++;
            newPosition += 40 * Setting::scaling.y;
        }
    }

    newPosition += 40 * Setting::scaling.y;
    newPosition += 40 * Setting::scaling.y;
    for( ; i < 12; i++)
    {
        texts[i]->setPosition(sf::Vector2f( window->getRealSize().x / 2 - texts[i]->getGlobalBounds().width / 2, newPosition));
        newPosition += texts[i]->getGlobalBounds().height + 20 * Setting::scaling.y;
        if( nextSpacing < numberSpacing && spacing[nextSpacing] == i )
        {
            nextSpacing++;
            newPosition += 40 * Setting::scaling.y;
        }
    }


}

Playable* Credits::play()
{
    clock.restart();
    while( window->isOpen())
    {
        sf::Event event;
        while (window->pollEvent(event))
        {
            switch (event.type)
            {
                case sf::Event::KeyPressed:
                    if( event.key.code == sf::Keyboard::Escape)
                    {
                        window->close();
                        return NULL;
                    }
                    else if( event.key.code == sf::Keyboard::Space)
                    {
                        StartMenu::getInstance(window)->addToRemove(this);
                        return StartMenu::getInstance(window);
                    }
                    break;

                case sf::Event::JoystickButtonPressed:
                    if( event.joystickButton.button == 0 || event.joystickButton.button == 1)
                    {
                        StartMenu::getInstance(window)->addToRemove(this);
                        return StartMenu::getInstance(window);
                    }
                    break;
            }
        }
        update( -clock.getElapsedTime().asSeconds() * 30 );
        clock.restart();
        window->clear();
        for( int i = 0; i < numberText; i++)
            window->draw(*texts[i]);
        window->display();
        if( texts[numberText-1]->getGlobalBounds().top + texts[numberText-1]->getGlobalBounds().height <= 0 )
            return StartMenu::getInstance(window);
    }
    return NULL;
}

void Credits::update(float offset)
{
    for( int i = 0; i < numberText ; i++)
        texts[i]->move(sf::Vector2f(0, offset));
}
