#include "StartMenu.h"
#include "MusicMixer.h"

#include "Game.h"
#include "Setting.h"
#include "Credits.h"
#include "PlayWindow.h"
#include "HelpMenu.h"
#include "ImageStorage.h"

StartMenu* StartMenu::startMenu = NULL;

StartMenu* StartMenu::getInstance(PlayWindow* window)
{
    if( startMenu == NULL )
        startMenu = new StartMenu(window, NULL);
    return startMenu;
}

StartMenu::StartMenu(PlayWindow* windowTemp, Playable* previous)
{
    if( previous != NULL )
        delete(previous);
    rotation = 0.02;
    switcher=0;
    offset = 0;
    index = 0;

    textureChar.loadFromFile("character_sheet.png");
    spriteChar.setTexture(textureChar);
    spriteChar.setScale(Setting::scaling.x, Setting::scaling.y);
    textureArm.loadFromFile("bazooka.png");
    spriteArm.setTexture(textureArm);
    spriteArm.setScale(Setting::scaling.x, Setting::scaling.y);
    textureButton.loadFromFile("A_button.png");
    spriteButton.setTexture(textureButton);


    image.loadFromFile("start_background.png");
    numberFrames = image.getSize().x / 1366;
    sprites = (sf::Sprite*)malloc(sizeof(sf::Sprite) * numberFrames);
    texture.loadFromImage(image, sf::IntRect(offset * 1366, 0, 1366, 768));

    for( int i = 0 ; i < numberFrames - 1; i++ )
    {
        // texture.loadFromImage(image, sf::IntRect(i * 1366, 0, 1366, 768));
        sprites[i].setTexture(texture);
    }

    texture.loadFromImage(image, sf::IntRect(offset * 1366, 0, 1366, 768));
    sprite.setTexture(texture);
    sprite.setScale(Setting::scaling.x, Setting::scaling.y);


    textureLogo.loadFromFile("logo.png");
    spriteLogo.setTexture(textureLogo);
    spriteLogo.setScale(Setting::scaling.x, Setting::scaling.y);
    spriteLogo.setOrigin(318,134);
    spriteButton.setOrigin(90,20);

    selectedItem = 0;
    window = windowTemp;

    counter=0;

    font.loadFromFile("Minecraftia.ttf");

    playText.setFont(font);
    playText.setCharacterSize(24 * Setting::scaling.y);
    playText.setString("Play");
    playText.setPosition( window->getRealSize().x / 2 - playText.getGlobalBounds().width / 2, window->getRealSize().y / 2 - playText.getGlobalBounds().height / 2  + 40 * Setting::scaling.y);
    playText.setColor(sf::Color(255, 255, 0, 255));

    spriteLogo.setPosition( window->getRealSize().x / 2, playText.getGlobalBounds().top - spriteLogo.getGlobalBounds().height / 2 - 40 * Setting::scaling.y);
    spriteChar.setPosition(328 * Setting::scaling.x, 235 * Setting::scaling.y);
    spriteArm.setPosition(365 * Setting::scaling.x, 274 * Setting::scaling.y);
    spriteButton.setPosition(window->getRealSize().x / 2 , window->getRealSize().y - 20 * Setting::scaling.y);

    creditsText.setFont(font);
    creditsText.setCharacterSize(24* Setting::scaling.y);
    creditsText.setString("Credits");
    creditsText.setPosition( window->getRealSize().x / 2 - creditsText.getGlobalBounds().width / 2, window->getRealSize().y / 2 - creditsText.getGlobalBounds().height / 2 + 80 * Setting::scaling.y);
    creditsText.setColor(sf::Color(255, 255, 255, 255));

    exitText.setFont(font);
    exitText.setCharacterSize(24 * Setting::scaling.y);
    exitText.setString("Exit");
    exitText.setPosition( window->getRealSize().x / 2 - exitText.getGlobalBounds().width / 2, window->getRealSize().y / 2 - exitText.getGlobalBounds().height / 2 + 160 * Setting::scaling.y);
    exitText.setColor(sf::Color(255, 255, 255, 255));

    helpText.setFont(font);
    helpText.setCharacterSize(24 * Setting::scaling.y);
    helpText.setString("Help");
    helpText.setPosition( window->getRealSize().x / 2 - exitText.getGlobalBounds().width / 2, window->getRealSize().y / 2 - exitText.getGlobalBounds().height / 2 + 120 * Setting::scaling.y);
    helpText.setColor(sf::Color(255, 255, 255, 255));

    rotateSwitch = spriteLogo.getRotation();
}

Playable* StartMenu::play()
{
    Playable::play();
    timeElapsed = 0;
    MusicMixer::playBackgroundMusic();
    while( window->isOpen())
    {
        xboxController(0);
        xboxController(1);
        sf::Event event;
        while (window->pollEvent(event))
        {
            switch (event.type)
            {
                case sf::Event::KeyPressed:
                    if( event.key.code == sf::Keyboard::Escape)
                    {
                        window->close();
                        return NULL;
                    }
                    else if( event.key.code == sf::Keyboard::Return)
                    {
                        if( selectedItem == 0)
                            return new Game(window, this);
                        else if ( selectedItem == 1)
                            return new Credits(window);
                        else if (selectedItem == 2)
                            return new HelpMenu(window);
                        else if ( selectedItem == 3)
                        {
                            window->close();
                            return NULL;
                        }

                    }
                    else if( event.key.code == sf::Keyboard::Down)
                    {
                        increment();
                        updateColorText();
                    }
                    else if( event.key.code == sf::Keyboard::Up)
                    {
                        decrement();
                        updateColorText();
                    }
                    break;

                case sf::Event::JoystickButtonPressed:
                    if( event.joystickButton.button == 0)
                    {
                        if( selectedItem == 0)
                            return new Game(window, this);
                        else if ( selectedItem == 1)
                            return new Credits(window);
                        else if (selectedItem == 2)
                            return new HelpMenu(window);
                        else if ( selectedItem == 3)
                        {
                            window->close();
                            return NULL;
                        }
                    }
                    break;
            }
        }

        timeElapsed += clock.getElapsedTime().asSeconds();
        clock.restart();

        if(timeElapsed > 0.1){
            spriteLogo.setRotation(spriteLogo.getRotation()+rotation);

            if(spriteLogo.getRotation() >= 5 && spriteLogo.getRotation() <= 20)
                rotation = -0.02;
            if(spriteLogo.getRotation() <= 355 && spriteLogo.getRotation()>=320)
                rotation = 0.02;

        }


        if( timeElapsed > START_MENU_ANIMATION_SPEED )
        {
            timeElapsed = 0;
            offset++;
            if( offset > numberFrames - 1 )
                offset = 0;
            texture.loadFromImage(image, sf::IntRect(offset * 1366, 0, 1366, 768));




            index++;
            if(index > 4)
                index = 0;

            if(index < 2 && index >=0)
                spriteArm.move(0,1);
            if(index>=2 && index <4)
                spriteArm.move(0,-1);
        }


        dimensionSprite.x = ImageStorage::getTexture(std::string("player"))->getSize().x / 5;
        dimensionSprite.y = ImageStorage::getTexture(std::string("player"))->getSize().y;
        spriteChar.setTextureRect(sf::IntRect(index * dimensionSprite.x, 0, dimensionSprite.x, dimensionSprite.y));

        window->clear();
        window->draw(sprite);
        window->draw(spriteLogo);
        window->draw(playText);
        window->draw(creditsText);
        window->draw(helpText);
        window->draw(exitText);
        window->draw(spriteChar);
        window->draw(spriteArm);
        window->draw(spriteButton);
        window->display();
    }
}

void StartMenu::increment()
{
    selectedItem++;
    if( selectedItem > 3 )
        selectedItem = 3;
    updateColorText();
}

void StartMenu::decrement()
{
    selectedItem--;
    if( selectedItem < 0 )
        selectedItem = 0;
    updateColorText();
}

void StartMenu::xboxController(int i)
{
    if( sf::Joystick::getAxisPosition(i, sf::Joystick::Y) > 80 )
    {
        if( !moving[i] )
            increment();
        moving[i] = true;
    }
    else if( sf::Joystick::getAxisPosition(i, sf::Joystick::Y) < -80 )
    {
        if( !moving[i] )
            decrement();
        moving[i] = true;
    }
    else if ( moving[i] )
    {
        moving[i] = false;
    }
}

void StartMenu::updateColorText()
{
    if( selectedItem == 0)
    {
        playText.setColor(sf::Color(255, 255, 0, 255));
        creditsText.setColor(sf::Color(255, 255, 255, 255));
        helpText.setColor(sf::Color(255,255,255,255));
        exitText.setColor(sf::Color(255, 255, 255, 255));
    }
    else if ( selectedItem == 1 )
    {
        playText.setColor(sf::Color(255, 255, 255, 255));
        creditsText.setColor(sf::Color(255, 255, 0, 255));
        helpText.setColor(sf::Color(255,255,255,255));
        exitText.setColor(sf::Color(255, 255, 255, 255));
    }
    else if ( selectedItem == 2 )
    {
        playText.setColor(sf::Color(255, 255, 255, 255));
        creditsText.setColor(sf::Color(255, 255, 255, 255));
        helpText.setColor(sf::Color(255,255,0,255));
        exitText.setColor(sf::Color(255, 255, 255, 255));
    }
    else if (selectedItem==3)
    {
        playText.setColor(sf::Color(255, 255, 255, 255));
        creditsText.setColor(sf::Color(255, 255, 255, 255));
        helpText.setColor(sf::Color(255,255,255,255));
        exitText.setColor(sf::Color(255, 255, 0, 255));
    }
}
